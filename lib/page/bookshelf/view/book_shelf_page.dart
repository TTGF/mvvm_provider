import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:mvvm_provider/base/multi_state_widget.dart';
import 'package:mvvm_provider/base/provider_selector_widget.dart';
import 'package:mvvm_provider/base/select_plus_data.dart';
import '../../../base/base_stateful_page.dart';
import '../../../base/common_refresher.dart';
import '../../../base/provider_consumer_widget.dart';
import '../../../routers/home_router.dart';
import '../../../routers/navigator_utils.dart';
import '../../../widgets/swiper_widget/swiper_widget.dart';
import '../../home/widgets/car_toon_widget.dart';
import '../view_model/book_shelf_view_model.dart';
import '../widgets/crosswise_widget.dart';
import 'package:collection/collection.dart';

class BookShelfPage extends BasePage {
  const BookShelfPage({super.key});

  @override
  BasePageState<BasePage> getState() => _BookShelfPageState();
}

class _BookShelfPageState extends BasePageState<BookShelfPage> {
  BookShelfViewModel bookShelfViewModel = BookShelfViewModel();

  @override
  void initState() {
    super.initState();
    super.pageTitle = '书架';
    isBack = false;
    _onRefresh();
  }

  @override
  Widget left() {
    return const SizedBox();
  }

  /// 请求分页
  int _pageNum = 1;

  /// 上拉加载
  void _onLoading() {
    _pageNum++;
    getListData();
  }

  /// 下拉刷新
  void _onRefresh() {
    _pageNum = 1;
    getListData();
  }

  void getListData() {
    bookShelfViewModel.getListData(
        getUrl(_pageNum), 'https://run.mocky.io/v3/f051dadd-a1fa-4edf-92ba-39f777754488', _pageNum);
  }

  String getUrl(int number) {
    String urlStr = '';
    if (_pageNum == 1) {
      urlStr = 'https://run.mocky.io/v3/59e3cc71-f43f-4d44-b81d-0d97b701569e';
    } else if (_pageNum == 2) {
      urlStr = 'https://run.mocky.io/v3/bdb6ae2d-2ceb-41e6-8361-3713320dcb07';
    } else if (_pageNum == 3) {
      urlStr = 'https://run.mocky.io/v3/bcc02347-ec86-4d2f-b9c5-62acddbde05b';
    } else {
      urlStr = '';
    }
    return urlStr;
  }

  /// 轮播图
  Widget _bannerWidget() {
    return SwiperWidget(
      imagesList: bookShelfViewModel.state.bannerList,
      width: 1.sw,
      height: 120.h,
      type: 1,
      onTap: (int index) {},
      indexChangeTap: (int index) {},
    );
  }

  /// 网格布局
  Widget _collectionWidget(int index) {
    return CarToonWidget(
      index: index,
      model: bookShelfViewModel.state.dataList[index],
      isStaggeredGrid: true,
      onTap: () {
        NavigatorUtils.push(context, HomeRouter.bookShelfDetailPage,
            arguments: {"imageUrl": bookShelfViewModel.state.dataList[index].image});
      },
    );
  }

  /// 横向布局
  Widget _crosswiseWidget(int index) {
    return CrosswiseWidget(
      model: bookShelfViewModel.state.dataList[index],
      onTap: () {
        NavigatorUtils.push(context, HomeRouter.bookShelfDetailPage,
            arguments: {"imageUrl": bookShelfViewModel.state.dataList[index].image});
      },
    );
  }

  @override
  Widget buildPage(BuildContext context) {
    return ProviderConsumerWidget<BookShelfViewModel>(
      viewModel: bookShelfViewModel,
      builder: (context, viewModel, child) {
        return MultiStateWidget(
            netState: viewModel.state.netState,
            placeHolderType: PlaceHolderType.staggeredGridPlaceHolder,
            builder: (BuildContext context) {
              return CommonRefresher(
                  enablePullDown: true,
                  enablePullUp: true,
                  refreshController: viewModel.refreshController,
                  onRefresh: _onRefresh,
                  onLoading: _onLoading,
                  listWidget: SingleChildScrollView(
                    controller: viewModel.scrollController,
                    child: StaggeredGrid.count(
                      crossAxisCount: 2,
                      mainAxisSpacing: 10.h,
                      crossAxisSpacing: 0,
                      children: [
                        ...viewModel.state.dataList.mapIndexed((index, tile) {
                          return StaggeredGridTile.extent(
                            crossAxisCellCount: tile.crossAxisCount,
                            mainAxisExtent:
                                index == 0 ? 200.h : (tile.crossAxisCount == 2 ? 180.h : 260.h),
                            child: index == 0
                                ? _bannerWidget()
                                : tile.crossAxisCount == 1
                                    ? _collectionWidget(index)
                                    : _crosswiseWidget(index),
                          );
                        }),
                      ],
                    ),
                  ));
            });
      },
    );
  }
}

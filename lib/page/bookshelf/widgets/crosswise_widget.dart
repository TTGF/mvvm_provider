import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../widgets/app_net_image.dart';
import '../../home/model/cartoon_model.dart';

class CrosswiseWidget extends StatelessWidget {
  final CartoonModel model;
  final Function onTap;

  const CrosswiseWidget({super.key, required this.model, required this.onTap});

  /// 图片
  Widget imageWidget() {
    return AppNetImage(
      fit: BoxFit.fitWidth,
      width: 1.sw,
      height: 180.h,
      imageUrl: model.image ?? '',
    );
  }

  /// 标题
  Widget titleWidget() {
    return Padding(
      padding: EdgeInsets.only(left: 10.w, right: 14.w),
      child: Text(
        model.title ?? '',
        overflow: TextOverflow.ellipsis,
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500, fontSize: 16.sp),
      ),
    );
  }

  /// 描述
  Widget desWidget() {
    String textStr = '';
    if ((model.comic_info!.tags ?? []).isNotEmpty) {
      if (model.comic_info!.main_style_name!.isNotEmpty) {
        textStr = "${model.comic_info?.main_style_name} | ${model.comic_info?.tags![0]}";
      } else {
        textStr = "${model.comic_info?.tags![0]}";
      }
    } else {
      if (model.comic_info!.main_style_name!.isEmpty) {
        textStr = "暂无描述";
      } else {
        textStr = model.comic_info?.main_style_name ?? "";
      }
    }
    return Text(
      textStr,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(color: Colors.black45, fontWeight: FontWeight.w500, fontSize: 14.sp),
    );
  }

  Widget otherWidget() {
    return Container(
      width: 1.sw,
      height: 40.h,
      color: Colors.white,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          titleWidget(),
          desWidget(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Stack(
        children: [imageWidget(), Positioned(bottom: 0, left: 0, child: otherWidget())],
      ),
      onTap: () {
        onTap();
      },
    );
  }
}

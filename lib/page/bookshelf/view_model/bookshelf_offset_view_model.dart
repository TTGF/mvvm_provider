import '../../../base/base_view_model.dart';
import '../states/book_shelf_detail_state.dart';

class BookShelfOffsetlViewModel extends BaseViewModel {
  BookShelfDetailState state = BookShelfDetailState();

  /// 改变间距和透明度
  void changeAppBarAlphaAndOffset(double alpha, double newOffset) {
    state.appBarAlpha = alpha;
    state.offset = newOffset;
    notifyListeners();
  }
}

import 'package:mvvm_provider/config/handle_state.dart';
import 'package:mvvm_provider/page/bookshelf/states/book_shelf_detail_state.dart';

import '../../../base/base_state.dart';
import '../../../base/base_view_model.dart';
import '../../../model/response_model.dart';
import '../../../net/ltt_https.dart';
import '../../home/model/cartoon_model_entity.dart';
import '../../home/model/cartoon_recommend_entity.dart';
import '../../home/model/cartoon_series_entity.dart';
import '../../../net/http_config.dart';

class BookShelfDetailViewModel extends BaseViewModel {
  BookShelfDetailState state = BookShelfDetailState();

  /// 请求全部数据
  Future<void> getData(
    String mainUrl,
    String seriesUrl,
    String recommendUrl,
  ) async {
    /// 请求主数据
    ResponseModel? responseMode1 = await LttHttp().request<CartoonModelData>(
        'https://run.mocky.io/v3/7b61a96f-e36d-4fc5-b2dd-a547bb3d935e',
        method: HttpConfig.get);
    state.netState1 = HandleState.handle(responseMode1, successCode: 0);
    if (state.netState1 == NetState.dataSuccessState) {
      state.mainModel = responseMode1.data;
    }

    /// 请求系列数据
    ResponseModel? responseModel2 = await LttHttp().request<CartoonSeriesData>(
        'https://run.mocky.io/v3/86e1609f-7823-4c50-afdd-3e764642fb02',
        method: HttpConfig.get);
    state.netState2 = HandleState.handle(responseModel2, successCode: 0);
    if (state.netState2 == NetState.dataSuccessState) {
      CartoonSeriesData cartoonSeriesData = responseModel2.data;
      state.seriesList = cartoonSeriesData.seriesComics;
    }

    /// 请求推荐数据
    ResponseModel? responseModel3 = await LttHttp().request<CartoonRecommendData>(
        'https://run.mocky.io/v3/232c1f4b-4d84-423d-9438-6d9ddf5034be',
        method: HttpConfig.get);
    state.netState3 = HandleState.handle(responseModel3, successCode: 0);
    if (state.netState3 == NetState.dataSuccessState) {
      CartoonRecommendData cartoonRecommendData = responseModel3.data;
      state.recommendList = cartoonRecommendData.infos;
    }

    if (state.netState1 == NetState.dataSuccessState &&
        state.netState2 == NetState.dataSuccessState &&
        state.netState3 == NetState.dataSuccessState) {
      state.netState = NetState.dataSuccessState;
    } else {
      state.netState = NetState.errorShowRefresh;
    }

    notifyListeners();
  }
}

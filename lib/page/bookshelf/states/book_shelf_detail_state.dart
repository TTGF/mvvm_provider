import 'package:mvvm_provider/base/base_state.dart';
import '../../home/model/cartoon_model_entity.dart';
import '../../home/model/cartoon_recommend_entity.dart';
import '../../home/model/cartoon_series_entity.dart';

class BookShelfDetailState extends BaseState {
  CartoonModelData? mainModel;
  List<CartoonRecommendDataInfos> recommendList = [];
  List<CartoonSeriesDataSeriesComics> seriesList = [];
  double appBarAlpha = 0;
  double offset = 0;
  NetState netState1 = NetState.loadingState;
  NetState netState2 = NetState.loadingState;
  NetState netState3 = NetState.loadingState;

  BookShelfDetailState() {
    mainModel = null;
    recommendList = [];
    seriesList = [];
    appBarAlpha = 0;
    offset = 0;
    netState1 = NetState.loadingState;
    netState2 = NetState.loadingState;
    netState3 = NetState.loadingState;
  }
}

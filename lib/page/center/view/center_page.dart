import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mvvm_provider/base/multi_state_widget.dart';
import '../../../base/base_grid_view.dart';
import '../../../base/base_stateful_page.dart';
import '../../../base/provider_consumer_widget.dart';
import '../../../routers/home_router.dart';
import '../../../routers/navigator_utils.dart';
import '../../home/widgets/car_toon_widget.dart';
import '../../mine/view_model/mine_view_model.dart';
import '../view_model/center_view_model.dart';

class CenterPage extends BasePage {
  const CenterPage({super.key});

  @override
  BasePageState<BasePage> getState() => _CenterPageState();
}

class _CenterPageState extends BasePageState<CenterPage> {
  CenterViewModel centerViewModel = CenterViewModel();

  @override
  void initState() {
    super.initState();
    super.pageTitle = '中心';
    isBack = false;
  }

  @override
  Widget left() {
    return const SizedBox();
  }

  /// 请求分页
  int _pageNum = 1;

  /// 上拉加载
  void _onLoading() {
    _pageNum++;
    getListData();
  }

  /// 下拉刷新
  void _onRefresh() {
    _pageNum = 1;
    getListData();
  }

  void getListData() {
    centerViewModel.getListData(getUrl(_pageNum), _pageNum);
  }

  String getUrl(int number) {
    String urlStr = '';
    if (_pageNum == 1) {
      urlStr = 'https://run.mocky.io/v3/59e3cc71-f43f-4d44-b81d-0d97b701569e';
    } else if (_pageNum == 2) {
      urlStr = 'https://run.mocky.io/v3/bdb6ae2d-2ceb-41e6-8361-3713320dcb07';
    } else if (_pageNum == 3) {
      urlStr = 'https://run.mocky.io/v3/bcc02347-ec86-4d2f-b9c5-62acddbde05b';
    } else {
      urlStr = '';
    }
    return urlStr;
  }

  @override
  Widget buildPage(BuildContext context) {
    return ProviderConsumerWidget<CenterViewModel>(
      viewModel: centerViewModel,
      onReady: (viewModel) {
        viewModel.getListData(getUrl(_pageNum), _pageNum);
      },
      builder: (context, viewModel, child) {
        return MultiStateWidget(
          netState: centerViewModel.state.netState,
          placeHolderType: PlaceHolderType.gridViewPlaceHolder,
          builder: (BuildContext context) {
            return Container(
              color: Colors.deepOrange,
              child: BaseGridView(
                enablePullDown: true,
                enablePullUp: true,
                onRefresh: _onRefresh,
                onLoading: _onLoading,
                refreshController: viewModel.refreshController,
                scrollController: viewModel.scrollController,
                data: viewModel.state.dataList ?? [],
                padding: EdgeInsets.all(10.h),
                childAspectRatio: 0.7,
                crossAxisSpacing: 10.w,
                mainAxisSpacing: 10.h,
                crossAxisCount: 2,
                bgColor: const Color(0xFFF3F4F8),
                itemBuilder: (context22, index) {
                  return CarToonWidget(
                    index: index,
                    model: viewModel.state.dataList![index],
                    onTap: () {
                      NavigatorUtils.push(context, HomeRouter.messageDetailPage);
                    },
                  );
                },
              ),
            );
          },
        );
      },
    );
  }
}

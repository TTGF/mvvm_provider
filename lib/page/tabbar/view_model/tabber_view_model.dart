import '../../../base/base_view_model.dart';

class TabberViewModel extends BaseViewModel {
  int selectIndex = 0;

  void changeSelectIndex(int index) {
    selectIndex = index;
    notifyListeners();
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mvvm_provider/page/bookshelf/view/book_shelf_page.dart';
import 'package:mvvm_provider/page/center/view/center_page.dart';
import 'package:mvvm_provider/page/mine/view/mine_page.dart';
import 'package:mvvm_provider/page/novel/view/novel_page.dart';
import '../../../base/provider_selector_widget.dart';
import '../../home/view/home_page.dart';
import '../view_model/tabber_view_model.dart';


class TabbarPage extends StatefulWidget {
  const TabbarPage({super.key});

  @override
  State<TabbarPage> createState() => _TabbarPageState();
}

class _TabbarPageState extends State<TabbarPage> {
  TabberViewModel tabberViewModel = TabberViewModel();
  @override
  Widget build(BuildContext context) {
    return ProviderSelectorWidget<TabberViewModel, int>(
      viewModel: tabberViewModel,
      selector: (context, index) => tabberViewModel.selectIndex,
      onReady: (viewModel) {
        viewModel.changeSelectIndex(0);
      },
      builder: (context, index, child) {
        return _buildPage(context, index);
      },
    );
  }

  Widget _buildPage(BuildContext context, int selectIndex) {
    List<Widget> tabPageList = [
      const HomePage(),
      const NovelPage(),
      const CenterPage(),
      const BookShelfPage(),
      const MinePage()
    ];

    /// 底部tab
    Widget buildBottomTab() {
      return BottomNavigationBar(
        unselectedItemColor: const Color(0xFF666666),
        selectedItemColor: const Color(0xFF000000),
        selectedFontSize: 11.sp,
        unselectedFontSize: 9.sp,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 12.h, bottom: 6.h),
              child: Image(
                fit: BoxFit.fill,
                width: 20.w,
                height: 20.w,
                image: AssetImage(selectIndex == 0
                    ? "assets/images/tabbar_home_select.png"
                    : "assets/images/tabbar_home_unselect.png"),
                color: selectIndex == 0 ? Colors.blueAccent : Colors.black38,
              ),
            ),
            label: '首页',
          ),
          BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.only(top: 12.h, bottom: 6.h),
                child: Image(
                  width: 20.w,
                  height: 20.w,
                  image: AssetImage(selectIndex == 1
                      ? "assets/images/tabbar_novel_select.png"
                      : "assets/images/tabbar_novel_unselect.png"),
                  color: selectIndex == 1 ? Colors.blueAccent : Colors.black38,
                ),
              ),
              label: '小说'),
          BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.only(top: 12.h, bottom: 6.h),
                child: Image(
                  width: 20.w,
                  height: 20.w,
                  image: const AssetImage("assets/images/tabbar_center.png"),
                  color: selectIndex == 1 ? Colors.red : Colors.grey,
                ),
              ),
              label: ''),
          BottomNavigationBarItem(
              icon: Stack(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      top: 12.h,
                      bottom: 6.h,
                    ),
                    child: Image(
                      width: 20.w,
                      height: 20.w,
                      image: AssetImage(selectIndex == 3
                          ? "assets/images/tabbar_book_select.png"
                          : "assets/images/tabbar_book_unselect.png"),
                      color: selectIndex == 3 ? Colors.blueAccent : Colors.black38,
                    ),
                  ),
                ],
              ),
              label: '书架'),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(top: 12.h, bottom: 6.h),
              child: Image(
                fit: BoxFit.fill,
                width: 20.w,
                height: 20.w,
                image: AssetImage(selectIndex == 4
                    ? "assets/images/tabbar_mine_select.png"
                    : "assets/images/tabbar_mine_unselect.png"),
                color: selectIndex == 4 ? Colors.blueAccent : Colors.black38,
              ),
            ),
            label: '我的',
          ),
        ],
        currentIndex: selectIndex,
        onTap: (int index) async {
          if (selectIndex == index) {
            return;
          }
          tabberViewModel.changeSelectIndex(index);
        },
      );
    }

    Container initFloatingActionButton() {
      return Container(
        margin: EdgeInsets.only(top: 56.h),
        width: 46.w,
        height: 46.h,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.blueAccent, width: 3.w),
            borderRadius: BorderRadius.circular(46.h)),
        child: FloatingActionButton(
          backgroundColor: Colors.white,
          elevation: 10,
          onPressed: () {
            tabberViewModel.changeSelectIndex(2);
          },
          child: Image(
            fit: BoxFit.cover,
            width: 46.w,
            height: 46.w,
            image: const AssetImage("assets/images/tabbar_center.png"),
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: IndexedStack(
        index: selectIndex,
        children: tabPageList,
      ),
      floatingActionButton: initFloatingActionButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: buildBottomTab(),
    );
  }
}

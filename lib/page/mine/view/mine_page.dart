import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvvm_provider/base/multi_state_widget.dart';
import 'package:mvvm_provider/routers/mine_router.dart';
import 'package:provider/provider.dart';
import '../../../base/base_list_view.dart';
import '../../../base/base_stateful_page.dart';
import '../../../base/provider_consumer_widget.dart';
import '../../../routers/navigator_utils.dart';
import '../model/info_model.dart';
import '../view_model/mine_view_model.dart';
import '../widgets/info_item.dart';

class MinePage extends BasePage {
  const MinePage({super.key});

  @override
  BasePageState<BasePage> getState() => _MinePageState();
}

class _MinePageState extends BasePageState<MinePage> {
  MineViewModel mineViewModel = MineViewModel();

  /// 请求分页
  int _pageNum = 1;

  @override
  void initState() {
    super.initState();
    super.pageTitle = '我的';
    pageBgColor = const Color(0xFFF3F4F8);
    isBack = false;
  }

  @override
  Widget left() {
    return const SizedBox();
  }

  /// 上拉加载
  void _onLoading() {
    _pageNum++;
    getListData();
  }

  /// 下拉刷新
  void _onRefresh() {
    _pageNum = 1;
    getListData();
  }

  void getListData() {
    mineViewModel.getListData('searchV5', _pageNum);
  }

  @override
  Widget buildPage(BuildContext context) {
    return ProviderConsumerWidget<MineViewModel>(
      viewModel: mineViewModel,
      onReady: (viewModel) {
        _onRefresh();
      },
      builder: (context, viewModel, child) {
        return MultiStateWidget(
          netState: mineViewModel.state.netState,
          placeHolderType: PlaceHolderType.listViewPlaceHolder,
          builder: (BuildContext context) {
            return BaseListView(
              refreshController: viewModel.refreshController,
              scrollController: viewModel.scrollController,
              bgColor: const Color(0xFFF3F4F8),
              enablePullDown: true,
              enablePullUp: true,
              data: viewModel.state.dataList,
              onRefresh: _onRefresh,
              onLoading: _onLoading,
              itemBuilder: (InfoModel model, int index) => InfoWidget(
                model: model,
                onTap: () {
                  NavigatorUtils.push(context, MineRouter.mineDetailPage, arguments: {
                    'entityId': model.id,
                  });
                },
              ),
            );
          },
        );
      },
    );
  }
}

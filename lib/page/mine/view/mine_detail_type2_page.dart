import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mvvm_provider/base/multi_state_widget.dart';
import 'package:mvvm_provider/page/mine/view_model/mine_detai_type2l_view_model.dart';
import 'package:status_bar_control/status_bar_control.dart';
import '../../../base/base_stateful_page.dart';
import '../../../base/provider_consumer_widget.dart';
import '../../../base/provider_selector_widget.dart';
import '../../../widgets/delay_button.dart';
import '../view_model/mine_detai_like_view_model.dart';
import '../widgets/article_nav_widget.dart';
import '../widgets/article_web_widget.dart';

/// 两个 viewModel 实现局部刷新

class MineDetailType2Page extends BasePage {
  final int entityId;
  const MineDetailType2Page({super.key, required this.entityId});

  @override
  BasePageState<BasePage> getState() => _MineDetailType2PageState();
}

class _MineDetailType2PageState extends BasePageState<MineDetailType2Page> {
  MineDetailType2ViewModel mineDetailType2ViewModel = MineDetailType2ViewModel();
  MineDetailLikeViewModel mineDetailLikeViewModel = MineDetailLikeViewModel();

  @override
  Widget left() {
    return const SizedBox();
  }

  @override
  void initState() {
    super.initState();
    isRenderHeader = false;
    pageBgColor = const Color(0xFFF3F4F8);
    isBack = false;
    _getData();
    Future.delayed(const Duration(milliseconds: 100)).then((value) async {
      StatusBarControl.setStyle(StatusBarStyle.DARK_CONTENT);
    });
  }

  /// 请求数据
  _getData() {
    mineDetailType2ViewModel.geDetailData('pc/items/info', widget.entityId);
  }

  /// 评论
  Widget commitWidget() {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(left: 18.w),
      margin: EdgeInsets.only(right: 18.w),
      width: 200.w,
      height: 30.h,
      decoration: BoxDecoration(
        color: const Color(0xFFF6F9FF),
        borderRadius: BorderRadius.all(Radius.circular(35.h)),
      ),
      child: Text(
        '想说点什么..',
        style: TextStyle(fontSize: 12.sp, color: const Color(0xFF999999)),
      ),
    );
  }

  /// 点赞
  Widget zanWidget() {
    return DelayButton(
        width: 40.w,
        height: 40.h,
        onTap: () {
          mineDetailLikeViewModel.changeLikeStatus(mineDetailType2ViewModel.state.model!);
        },
        mainWidget: Column(
          children: [
            Image.asset(
              "assets/images/zan_nonal.png",
              fit: BoxFit.fitWidth,
              width: 18.w,
              height: 18.w,
              color: (mineDetailType2ViewModel.state.model?.isLike ?? false) == false
                  ? Colors.blueGrey
                  : Colors.red,
            ),
            SizedBox(
              height: 5.h,
            ),
            Text(
              '点赞',
              style: TextStyle(fontSize: 12.sp, color: const Color(0xFF999999)),
            ),
          ],
        ));
  }

  Widget articleTooWidget() {
    return Container(
      width: 1.sw,
      height: 60.h,
      padding: EdgeInsets.only(left: 18.w, right: 18.w),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [commitWidget(), zanWidget()],
      ),
    );
  }

  Widget mainWidget() {
    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.only(top: statusBarH + kToolbarHeight, bottom: bottomSafeBarH + 60.h),
          child: ArticleWebWidget(
            htmlStr: mineDetailType2ViewModel.state.model!.newsContent,
          ),
        ),
        Positioned(
            child: ArticleNavWidget(
          infoListModel: mineDetailType2ViewModel.state.model!,
        )),
        ProviderSelectorWidget<MineDetailLikeViewModel, bool>(
            viewModel: mineDetailLikeViewModel,
            isValue: true,
            builder: (context, isLike, child) {
              return Positioned(bottom: bottomSafeBarH, child: articleTooWidget());
            },
            selector: (context, viewModel) => mineDetailLikeViewModel.state.isLike),
        Center(
          child: Container(
            padding: EdgeInsets.all(10.w),
            color: Colors.blue,
            child: Text(
              '局部刷新,点赞功能案例 实现方式二',
              style: TextStyle(color: Colors.white, fontSize: 16.sp),
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget buildPage(BuildContext context) {
    return ProviderConsumerWidget(
        viewModel: mineDetailType2ViewModel,
        builder: (context, viewModel, child) {
          return MultiStateWidget(
              netState: viewModel.state.netState,
              placeHolderType: PlaceHolderType.noPlaceHolder,
              builder: (BuildContext context) {
                return mainWidget();
              });
        });
  }
}

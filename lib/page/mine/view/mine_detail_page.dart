import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mvvm_provider/base/base_state.dart';
import 'package:status_bar_control/status_bar_control.dart';
import '../../../base/base_stateful_page.dart';
import '../../../base/multi_state_widget.dart';
import '../../../base/provider_selector_widget.dart';
import '../../../routers/mine_router.dart';
import '../../../routers/navigator_utils.dart';
import '../../../widgets/delay_button.dart';
import '../view_model/mine_detail_view_model.dart';
import '../widgets/article_nav_widget.dart';
import '../widgets/article_web_widget.dart';

class MineDetailPage extends BasePage {
  final int entityId;
  const MineDetailPage({super.key, required this.entityId});

  @override
  BasePageState<BasePage> getState() => _MineDetailPageState();
}

class _MineDetailPageState extends BasePageState<MineDetailPage> {
  MineDetailViewModel mineDetailViewModel = MineDetailViewModel();

  @override
  Widget left() {
    return const SizedBox();
  }

  @override
  void initState() {
    super.initState();
    isRenderHeader = false;
    pageBgColor = const Color(0xFFF3F4F8);
    isBack = false;
    _getData();
    Future.delayed(const Duration(milliseconds: 100)).then((value) async {
      StatusBarControl.setStyle(StatusBarStyle.DARK_CONTENT);
    });
  }

  /// 请求数据
  _getData() {
    mineDetailViewModel.geDetailData('pc/items/info', widget.entityId);
  }

  /// 评论
  Widget commitWidget() {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(left: 18.w),
      margin: EdgeInsets.only(right: 18.w),
      width: 200.w,
      height: 30.h,
      decoration: BoxDecoration(
        color: const Color(0xFFF6F9FF),
        borderRadius: BorderRadius.all(Radius.circular(35.h)),
      ),
      child: Text(
        '想说点什么..',
        style: TextStyle(fontSize: 12.sp, color: const Color(0xFF999999)),
      ),
    );
  }

  /// 点赞
  Widget zanWidget() {
    return DelayButton(
        width: 40.w,
        height: 40.h,
        onTap: () {
          mineDetailViewModel.changeLikeStatus();
        },
        mainWidget: Column(
          children: [
            Image.asset(
              "assets/images/zan_nonal.png",
              fit: BoxFit.fitWidth,
              width: 18.w,
              height: 18.w,
              color: (mineDetailViewModel.state.model?.isLike ?? false) == false
                  ? Colors.blueGrey
                  : Colors.red,
            ),
            SizedBox(
              height: 5.h,
            ),
            Text(
              '点赞',
              style: TextStyle(fontSize: 12.sp, color: const Color(0xFF999999)),
            ),
          ],
        ));
  }

  Widget articleTooWidget() {
    return Container(
      width: 1.sw,
      height: 60.h,
      padding: EdgeInsets.only(left: 18.w, right: 18.w),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [commitWidget(), zanWidget()],
      ),
    );
  }

  Widget mainWidget() {
    return Stack(
      children: [
        ProviderSelectorWidget<MineDetailViewModel, int>(
            viewModel: mineDetailViewModel,
            isValue: true,
            builder: (context, entityId, child) {
              return MultiStateWidget(
                  netState: mineDetailViewModel.state.netState,
                  placeHolderType: PlaceHolderType.detailPlaceHolder,
                  builder: (BuildContext context) {
                    return Padding(
                      padding: EdgeInsets.only(
                          top: statusBarH + kToolbarHeight, bottom: bottomSafeBarH + 60.h),
                      child: ArticleWebWidget(
                        htmlStr: mineDetailViewModel.state.model!.newsContent,
                      ),
                    );
                  });
            },
            selector: (context, bookShelfViewModel) =>
                mineDetailViewModel.state.model?.entityId ?? 0),
        ProviderSelectorWidget<MineDetailViewModel, int>(
            viewModel: mineDetailViewModel,
            isValue: true,
            builder: (context, selectorPlusData, child) {
              return mineDetailViewModel.state.netState == NetState.dataSuccessState
                  ? Positioned(
                      child: ArticleNavWidget(
                      infoListModel: mineDetailViewModel.state.model!,
                    ))
                  : const SizedBox();
            },
            selector: (context, bookShelfViewModel) =>
                mineDetailViewModel.state.model?.entityId ?? 0),
        ProviderSelectorWidget<MineDetailViewModel, bool>(
            viewModel: mineDetailViewModel,
            isValue: true,
            builder: (context, selectorPlusData, child) {
              return Positioned(bottom: bottomSafeBarH, child: articleTooWidget());
            },
            selector: (context, bookShelfViewModel) =>
                mineDetailViewModel.state.model?.isLike ?? false),
        Center(
          child: Container(
            padding: EdgeInsets.all(10.w),
            color: Colors.blue,
            height: 100.h,
            child: Column(
              children: [
                Text(
                  '局部刷新,点赞功能案例 实现方式一',
                  style: TextStyle(color: Colors.white, fontSize: 16.sp),
                ),
                DelayButton(
                    width: 140.w,
                    height: 40.h,
                    decoration: const BoxDecoration(color: Colors.cyanAccent),
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(top: 10.h),
                    onTap: () {
                      NavigatorUtils.push(context, MineRouter.mineDetailType2Page, arguments: {
                        'entityId': widget.entityId,
                      });
                    },
                    mainWidget: Text(
                      '点击查看方式二',
                      style: TextStyle(fontSize: 15.sp, color: Colors.red),
                    ))
              ],
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget buildPage(BuildContext context) {
    return mainWidget();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    mineDetailViewModel.dispose();
  }
}

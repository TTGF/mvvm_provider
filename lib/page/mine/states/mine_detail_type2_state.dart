import 'package:mvvm_provider/base/base_state.dart';
import '../model/info_model.dart';

class MineDetailType2State extends BaseState {
  InfoListModel? model;
  bool isLike = false;

  MineDetailType2State() {
    model = null;
    isLike = false;
  }
}

import 'package:mvvm_provider/base/base_state.dart';
import 'package:mvvm_provider/config/handle_state.dart';
import 'package:mvvm_provider/page/mine/states/mine_state.dart';
import '../../../base/base_view_model.dart';
import '../../../model/response_model.dart';
import '../../../net/ltt_https.dart';
import '../../../net/http_config.dart';
import '../model/info_model.dart';

class MineViewModel extends BaseViewModel {

  MineState state = MineState();
  /// 获取数据
  Future<void> getListData(String path, int pageNum) async {
    ResponseModel? responseModel = await LttHttp().request<InfoListModel>(path,
        params: {"pn": pageNum, "ps": 20, "q": "车讯原创", "t": 0}, method: HttpConfig.get);
    /// 处理页面状态
    state.netState = HandleState.handle(responseModel);
    if (state.netState == NetState.dataSuccessState) {
      /// 数据请求成功
      InfoListModel infoListModel = responseModel.data;
      if (pageNum == 1) {
        state.dataList = infoListModel.works;
        if (state.dataList.isEmpty) {
          state.netState = NetState.emptyDataState;
        }
      } else {
        state.dataList.addAll(infoListModel.works);
        /// 显示没有更多数据了x
        if (infoListModel.works.isEmpty) {
          refreshController.loadNoData();
        }
      }
      refreshController.refreshCompleted();
      refreshController.loadComplete();
    }
    notifyListeners();
  }
}

import 'package:mvvm_provider/base/base_state.dart';
import 'package:mvvm_provider/page/mine/states/mine_detail_state.dart';
import '../../../base/base_view_model.dart';
import '../../../config/handle_state.dart';
import '../../../model/response_model.dart';
import '../../../net/ltt_https.dart';
import '../../../net/http_config.dart';
import '../model/info_model.dart';

class MineDetailViewModel extends BaseViewModel {

  MineDetailState state = MineDetailState();
  /// 获取详情数据
  Future<void> geDetailData(String path, int entityId) async {
    ResponseModel? responseModel = await LttHttp()
        .request<InfoListModel>(path, params: {"entityId": entityId}, method: HttpConfig.get);
    state.netState = HandleState.handle(responseModel);
    if (state.netState == NetState.dataSuccessState) {
      state.model = responseModel.data;
    }
    notifyListeners();
  }

  void changeLikeStatus() {
    state.model?.isLike = !state.model!.isLike;
    notifyListeners();
  }
}

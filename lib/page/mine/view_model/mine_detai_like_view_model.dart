import 'package:mvvm_provider/page/mine/states/mine_detail_type2_state.dart';
import '../../../base/base_view_model.dart';
import '../model/info_model.dart';

class MineDetailLikeViewModel extends BaseViewModel {
  MineDetailType2State state = MineDetailType2State();

  /// 点赞
  void changeLikeStatus(InfoListModel model) {
    state.isLike = model.isLike = !model.isLike;
    notifyListeners();
  }
}

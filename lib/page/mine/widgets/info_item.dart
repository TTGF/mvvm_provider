import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../widgets/app_net_image.dart';
import '../model/info_model.dart';

class InfoWidget extends StatelessWidget {
  final InfoModel model;
  Function onTap;
  InfoWidget({super.key, required this.model, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap();
      },
      child: Container(
        width: 1.sw,
        color: Colors.white,
        padding: EdgeInsets.only(left: 10.w, right: 10.w, top: 14.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            threeImageWidget(),
            SizedBox(
              height: 10.h,
            ),
            titleWidget(),
            SizedBox(
              height: 10.h,
            ),
            bottowWidget(),
            SizedBox(
              height: 14.h,
            ),
            lineWidget()
          ],
        ),
      ),
    );
  }

  /// 三张图片
  Widget threeImageWidget() {
    List<Widget> rowWidget = [];
    if (model.newsPics.length >= 3) {
      for (int i = 0; i < model.newsPics.length; i++) {
        rowWidget.add(
          AppNetImage(
            imageUrl: model.newsPics[i],
            width: (1.sw - 40.w) / 3,
            height: (1.sw - 40.w) / 3 / 3 * 2,
            radius: 5.h,
          ),
        );
        if (i < 2) {
          rowWidget.add(
            SizedBox(
              width: 10.w,
            ),
          );
        }
      }
    } else {
      rowWidget.add(AppNetImage(
        imageUrl: model.videoCover,
        width: (1.sw - 20.w),
        height: (1.sw - 20.w) / 3 * 2,
        radius: 5.h,
      ));
    }
    return Row(
      children: rowWidget,
    );
  }

  /// 文字描述
  Widget titleWidget() {
    return Text(
      model.title,
    );
  }

  /// 底部viw
  Widget bottowWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [authorWidget(), toolWidget()],
    );
  }

  /// 作者
  Widget authorWidget() {
    return Row(
      children: [
        AppNetImage(
          imageUrl: model.mcnIcon,
          width: 20.w,
          height: 20.w,
          radius: 10.w,
        ),
        SizedBox(
          width: 5.w,
        ),
        Text(
          model.mcnRealName,
        ),
        SizedBox(
          width: 5.w,
        ),
        Text(
          model.createTimeStr,
        ),
      ],
    );
  }

  /// 工具栏
  Widget toolWidget() {
    return Row(
      children: [
        Row(
          children: [
            Image.asset(
              "assets/images/pageviewIcon.png",
              fit: BoxFit.fitWidth,
              width: 20.w,
              height: 20.w,
            ),
            SizedBox(
              width: 5.w,
            ),
            Text(
              model.uVContentStr,
            ),
          ],
        ),
        SizedBox(
          width: 10.w,
        ),
        GestureDetector(
          onTap: () {},
          child: Row(
            children: [
              Image.asset(
                "assets/images/zan_nonal.png",
                fit: BoxFit.fitWidth,
                width: 16.w,
                height: 16.w,
                color: model.isThumbs == 1 ? Colors.red : Colors.blueGrey,
              ),
              SizedBox(
                width: 5.w,
              ),
              Text(
                '点赞',
              ),
            ],
          ),
        )
      ],
    );
  }

  /// 线
  Widget lineWidget() {
    return Container(
      height: 1.h,
      width: 1.sw - 20.w,
      color: Colors.black12,
    );
  }
}

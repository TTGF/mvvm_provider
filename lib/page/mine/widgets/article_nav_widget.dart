import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../routers/navigator_utils.dart';
import '../../../widgets/app_net_image.dart';
import '../../../widgets/delay_button.dart';
import '../model/info_model.dart';

class ArticleNavWidget extends StatelessWidget {
  final InfoListModel infoListModel;
  const ArticleNavWidget({super.key,required this.infoListModel});

  @override
  Widget build(BuildContext context) {
    /// 导航栏
    return Container(
      padding: EdgeInsets.only(right: 15.w, top: ScreenUtil().statusBarHeight + 5.h),
      width: 1.sw,
      color: const Color(0xFFF6F9FF),
      height: ScreenUtil().statusBarHeight + kToolbarHeight,
      alignment: Alignment.center,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              DelayButton(
                width: 40.w,
                height: ScreenUtil().statusBarHeight + kToolbarHeight,
                onTap: () {
                  NavigatorUtils.pop(context);
                },
                mainWidget: Align(
                  child: Image.asset("assets/images/back_black.png",
                      fit: BoxFit.cover, color: Colors.blueGrey, width: 26.w, height: 26.w),
                ),
              ),
              AppNetImage(
                imageUrl: infoListModel.mcnIcon ?? '',
                width: 36.w,
                height: 36.w,
                radius: 18.w,
              ),
              SizedBox(width: 8.w),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    infoListModel.mcnRealName ?? '',
                    style: TextStyle(
                        color: Colors.black87, fontWeight: FontWeight.w600, fontSize: 15.sp),
                  ),
                  Text(
                    '浏览量 ${infoListModel.pageViewStr ?? ''}  粉丝 ${infoListModel.mcnFansCount ?? ''}',
                    style: TextStyle(
                        color: Colors.black45, fontSize: 13.sp, fontWeight: FontWeight.w400),
                  ),
                ],
              )
            ],
          ),
          DelayButton(
            width: 50.w,
            height: 26.h,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(13.h)),
              border: Border.all(width: 1, color: Colors.black54),
            ),
            onTap: () {},
            mainWidget: Text(
              '关注',
              style: TextStyle(color: Colors.black, fontSize: 12.sp, fontWeight: FontWeight.w400),
            ),
          ),
        ],
      ),
    );
  }
}

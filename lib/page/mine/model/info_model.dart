import 'dart:convert';
import '../../../generated/json/base/json_field.dart';
import '../../../generated/json/info_model.g.dart';

@JsonSerializable()
class InfoListModel {
  List<InfoModel> works = [];
  String newsContent = '';
  String cover = '';
  int shareCount = 0;
  String mcnIcon = '';
  String editorName = '';
  int mcnTemplateId = 0;
  int followStatus = 0;
  int entityType = 0;

  String nickName = '';
  bool isThumbs = false;
  int commentCount = 0;
  String pageViewStr = '';
  String shareCountStr = '';
  int subType = 0;

  // List newsPics = [];

  int status = 0;

  String mcnRealName = '';
  String creationTime = '';
  bool isLike = false;
  String userAvatar = '';

  String mcnPageView = '';
  String description = '';
  String commentCountStr = '';
  String title = '';
  String mcnFansCount = '';

  String brandLogo = '';
  int channelId = 0;
  String thumbsCounts = '';
  int original = 0;
  String uVContent = '';
  int entityId = 0;

  String userName = '';

  String userId = '';
  String url = '';
  int pageView = 0;
  String createTime = '';
  int mcnId = 0;
  int categoryId = 0;

  InfoListModel();
  factory InfoListModel.fromJson(Map<String, dynamic> json) => $InfoListModelFromJson(json);
  Map<String, dynamic> toJson() => $InfoListModelToJson(this);
  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class InfoModel {
  String mcnRealName = '';
  String uVContentStr = '';
  int adFlag = 0;
  String description = '';
  String title = '';
  int checkStatus = 0;
  String mcnIcon = '';
  int adFlagShow = 0;
  int adsType = 0;
  int worksType = 0;
  int id = 0;
  int uVContent = 0;
  String avatarUrl = '';
  int entityType = 0;
  String nickName = '';
  int userId = 0;
  String url = '';
  String createTime = '';
  String createTimeStr = '';
  String videoCover = '';
  List<String> newsPics = [];
  int isThumbs = 0;
  InfoModel();
  factory InfoModel.fromJson(Map<String, dynamic> json) => $InfoModelFromJson(json);
  Map<String, dynamic> toJson() => $InfoModelToJson(this);
  @override
  String toString() {
    return jsonEncode(this);
  }
}

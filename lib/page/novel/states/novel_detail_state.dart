import 'package:mvvm_provider/base/base_state.dart';
import '../model/content_model_entity.dart';
import '../model/tabs_model_entity.dart';

class NovelDetailState extends BaseState {
  List<TabsModelDataList> tabsList = [];
  List<ContentModelDataList> contentList = [];
  int selectIndex = 0;
  NovelDetailState() {
    tabsList = [];
    contentList = [];
    selectIndex = 0;
  }
}

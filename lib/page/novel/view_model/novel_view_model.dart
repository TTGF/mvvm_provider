import 'package:mvvm_provider/base/base_state.dart';
import 'package:mvvm_provider/page/novel/states/novel_state.dart';
import '../../../base/base_view_model.dart';
import '../../../config/handle_state.dart';
import '../../../model/response_model.dart';
import '../../../net/ltt_https.dart';
import '../../../net/http_config.dart';
import '../../../widgets/easy_loading.dart';
import '../../mine/model/info_model.dart';

class NovelViewModel extends BaseViewModel {
  NovelState state = NovelState();

  /// 获取数据
  Future<void> getListData(String path, int pageNum) async {
    ResponseModel? responseModel = await LttHttp().request<InfoListModel>(path,
        params: {"pn": pageNum, "ps": 20, "q": "车讯原创", "t": 0}, method: HttpConfig.get);

    /// 处理页面状态
    state.netState = HandleState.handle(responseModel);
    if (state.netState == NetState.dataSuccessState) {
      /// 数据请求成功
      InfoListModel infoListModel = responseModel.data;
      state.dataList = infoListModel.works;
    }
    notifyListeners();
  }

  /// 删除数据操作
  void removeData(int index) {
    if (index >= state.dataList.length) return;
    state.dataList.removeAt(index);
    notifyListeners();
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mvvm_provider/base/multi_state_widget.dart';
import 'package:mvvm_provider/base/select_plus_data.dart';
import 'package:mvvm_provider/routers/mine_router.dart';
import 'package:mvvm_provider/widgets/delay_button.dart';
import '../../../base/base_list_view.dart';
import '../../../base/base_stateful_page.dart';
import '../../../base/provider_consumer_widget.dart';
import '../../../base/provider_selector_widget.dart';
import '../../../routers/navigator_utils.dart';
import '../../mine/model/info_model.dart';
import '../../mine/widgets/info_item.dart';
import '../view_model/novel_view_model.dart';

class NovelPage extends BasePage {
  const NovelPage({super.key});

  @override
  BasePageState<BasePage> getState() => _NovelPageState();
}

class _NovelPageState extends BasePageState<NovelPage> {
  NovelViewModel novelViewModel = NovelViewModel();

  @override
  void initState() {
    super.initState();
    super.pageTitle = 'Selector用法案例';
    isBack = false;
  }

  @override
  Widget left() {
    return const SizedBox();
  }

  void getListData() {
    novelViewModel.getListData('searchV5', 1);
  }

  /// 删除widget
  Widget removeWidget(int index) {
    return Positioned(
        right: 15.w,
        bottom: 15.h,
        child: DelayButton(
            width: 60.w,
            height: 30.h,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: Colors.red, borderRadius: BorderRadius.all(Radius.circular(6.h))),
            onTap: () {
              novelViewModel.removeData(index);
            },
            mainWidget: Text(
              '删除',
              style: TextStyle(color: Colors.white, fontSize: 14.sp),
            )));
  }

  @override
  Widget buildPage(BuildContext context) {
    return ProviderSelectorWidget<NovelViewModel, List>(
        viewModel: novelViewModel,
        onReady: (viewModel) {
          getListData();
        },
        builder: (context, selectorPlusData, child) {
          return MultiStateWidget(
              placeHolderType: PlaceHolderType.listViewPlaceHolder,
              netState: novelViewModel.state.netState,
              refreshMethod: () {
                getListData();
              },
              builder: (BuildContext context) {
                return ListView.builder(
                  cacheExtent: 20,
                  physics: const BouncingScrollPhysics(),
                  reverse: false,
                  itemCount: selectorPlusData.value.length,
                  itemBuilder: (context, index) {
                    return Stack(
                      children: [
                        InfoWidget(
                          model: selectorPlusData.value[index],
                          onTap: () {
                            NavigatorUtils.push(context, MineRouter.novelDetailPage);
                          },
                        ),
                        removeWidget(index)
                      ],
                    );
                  },
                );
              });
        },
        plusDataSelector: (context, viewModel) =>
            SelectorPlusData(value: novelViewModel.state.dataList));
  }
}

import 'package:mvvm_provider/generated/json/base/json_field.dart';
import 'package:mvvm_provider/generated/json/content_model_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class ContentModelData {
	late List<ContentModelDataList> list;
	@JSONField(name: "next_time")
	late String nextTime;
	@JSONField(name: "current_time")
	late String currentTime;
	late String text;
	late List<dynamic> last;
	@JSONField(name: "end_text")
	late String endText;
	@JSONField(name: "horizontal_cover")
	late String horizontalCover;
	late String description;
	late String title;

	ContentModelData();

	factory ContentModelData.fromJson(Map<String, dynamic> json) => $ContentModelDataFromJson(json);

	Map<String, dynamic> toJson() => $ContentModelDataToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class ContentModelDataList {
	@JSONField(name: "comic_id")
	late int comicId;
	late String title;
	late List<String> author;
	@JSONField(name: "vertical_cover")
	late String verticalCover;
	@JSONField(name: "is_finish")
	late int isFinish;
	@JSONField(name: "last_ord")
	late int lastOrd;
	@JSONField(name: "last_short_title")
	late String lastShortTitle;
	late List<ContentModelDataListStyles> styles;
	late int total;
	late String fans;
	@JSONField(name: "reward_users")
	late List<dynamic> rewardUsers;
	@JSONField(name: "last_rank")
	late int lastRank;
	@JSONField(name: "last_ep")
	late int lastEp;
	late String text;
	@JSONField(name: "rookie_type")
	late int rookieType;
	@JSONField(name: "allow_wait_free")
	late bool allowWaitFree;
	@JSONField(name: "discount_type")
	late int discountType;
	@JSONField(name: "home_block_jump_value")
	late String homeBlockJumpValue;

	ContentModelDataList();

	factory ContentModelDataList.fromJson(Map<String, dynamic> json) => $ContentModelDataListFromJson(json);

	Map<String, dynamic> toJson() => $ContentModelDataListToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class ContentModelDataListStyles {
	late int id;
	late String name;

	ContentModelDataListStyles();

	factory ContentModelDataListStyles.fromJson(Map<String, dynamic> json) => $ContentModelDataListStylesFromJson(json);

	Map<String, dynamic> toJson() => $ContentModelDataListStylesToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}
import 'package:mvvm_provider/generated/json/base/json_field.dart';
import 'package:mvvm_provider/generated/json/tabs_model_entity.g.dart';
import 'dart:convert';


@JsonSerializable()
class TabsModelData {
	late List<TabsModelDataList> list;
	@JSONField(name: "default_id")
	late int defaultId;

	TabsModelData();

	factory TabsModelData.fromJson(Map<String, dynamic> json) => $TabsModelDataFromJson(json);

	Map<String, dynamic> toJson() => $TabsModelDataToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class TabsModelDataList {
	late int id;
	late int type;
	late String description;
	late String name;

	TabsModelDataList();

	factory TabsModelDataList.fromJson(Map<String, dynamic> json) => $TabsModelDataListFromJson(json);

	Map<String, dynamic> toJson() => $TabsModelDataListToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}
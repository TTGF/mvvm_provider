import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../widgets/app_net_image.dart';
import '../model/content_model_entity.dart';

class NovelContentItem extends StatelessWidget {
  final ContentModelDataList model;
  final int index;
  const NovelContentItem({super.key, required this.model, required this.index});

  /// 排行数字
  Widget _numberWidget() {
    double fontSize;
    String indexStr;
    if (index <= 3) {
      fontSize = 70.sp;
      indexStr = '$index';
    } else {
      fontSize = 30.sp;
      if (index < 10) {
        indexStr = '0$index';
      } else {
        indexStr = '$index';
      }
    }

    return SizedBox(
      width: 44.w,
      child: Text(
        indexStr,
        style: TextStyle(
            fontSize: fontSize, fontWeight: FontWeight.w600, color: const Color(0xFFF7B500)),
      ),
    );
  }

  /// 图片
  Widget _imageWidget() {
    return AppNetImage(
      imageUrl: model.verticalCover,
      width: 120.w,
      height: 140.w,
      fit: BoxFit.fitWidth,
      radius: 8.h,
    );
  }

  /// 标题
  Widget _titleWidget(String title, int type) {
    TextStyle blackStyle =
        TextStyle(fontSize: 14.sp, fontWeight: FontWeight.w600, color: const Color(0xFF000000));
    TextStyle qianStyle = TextStyle(fontSize: 12.sp, color: const Color(0xFF999999));

    return SizedBox(
      width: 1.sw - 275.w,
      child: Text(
        title,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        style: type == 1 ? blackStyle : qianStyle,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20.h, left: 5.w),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _numberWidget(),
          _imageWidget(),
          SizedBox(
            width: 10.w,
          ),
          Column(
            children: [
              SizedBox(
                height: 10.h,
              ),
              _titleWidget(model.title, 1),
              SizedBox(
                height: 30.h,
              ),
              _titleWidget(model.author.isNotEmpty ? model.author[0] : '暂无数据', 2),
              SizedBox(
                height: 6.h,
              ),
              _titleWidget(model.styles.isNotEmpty ? model.styles[0].name : '暂无数据', 2),
              SizedBox(
                height: 6.h,
              ),
              _titleWidget('更新至${model.lastShortTitle}话', 2),
            ],
          )
        ],
      ),
    );
  }
}

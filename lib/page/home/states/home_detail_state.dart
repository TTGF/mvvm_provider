import 'package:mvvm_provider/base/base_state.dart';
import '../model/cartoon_model_entity.dart';
import '../model/cartoon_recommend_entity.dart';
import '../model/cartoon_series_entity.dart';

class HomeDetailState extends BaseState {
  CartoonModelData? mainModel;
  List<CartoonRecommendDataInfos> recommendList = [];
  List<CartoonSeriesDataSeriesComics> seriesList = [];
  double appBarAlpha = 0;
  double offset = 0;

  HomeDetailState() {
    mainModel = null;
    recommendList = [];
    seriesList = [];
    appBarAlpha = 0;
    offset = 0;
  }
}

import 'package:mvvm_provider/base/base_state.dart';

import '../model/cartoon_model.dart';

class HomeState extends BaseState{
  List<CartoonModel>? dataList;
  bool isLike = false;

  HomeState() {
    dataList = [];
    isLike = false;
  }
}

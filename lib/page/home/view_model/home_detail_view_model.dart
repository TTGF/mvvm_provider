import 'package:mvvm_provider/base/base_state.dart';
import 'package:mvvm_provider/page/home/states/home_detail_state.dart';

import '../../../base/base_view_model.dart';
import '../../../model/response_model.dart';
import '../../../net/ltt_https.dart';
import '../../../widgets/easy_loading.dart';
import '../model/cartoon_model_entity.dart';
import '../model/cartoon_recommend_entity.dart';
import '../model/cartoon_series_entity.dart';
import '../../../net/http_config.dart';

class HomeDetailViewModel extends BaseViewModel {
  HomeDetailState homeDetailState = HomeDetailState();

  /// 改变间距和透明度
  void changeAppBarAlphaAndOffset(double alpha, double newOffset) {
    homeDetailState.appBarAlpha = alpha;
    homeDetailState.offset = newOffset;
    notifyListeners();
  }

  /// 请求全部数据
  getAllData() async {
    await Future.wait<dynamic>([getMainData(), getSeriesData(), getRecommendData()]).then((value) {
      if (value[0] == null || value[1] == null || value[2] == null) {
        homeDetailState.netState = NetState.errorShowRefresh;
        notifyListeners();
        return;
      }
      homeDetailState.mainModel = value[0];
      homeDetailState.seriesList = value[1];
      homeDetailState.recommendList = value[2];
      homeDetailState.netState = NetState.dataSuccessState;
      notifyListeners();
    }).catchError((error) {
      homeDetailState.netState = NetState.errorShowRefresh;
      notifyListeners();
    });
  }

  /// 请求主数据
  getMainData() async {
    ResponseModel? responseModel = await LttHttp().request<CartoonModelData>(
        'https://run.mocky.io/v3/7b61a96f-e36d-4fc5-b2dd-a547bb3d935e',
        method: HttpConfig.get);
    return responseModel.data;
  }

  /// 请求系列数据
  getSeriesData() async {
    ResponseModel? responseModel = await LttHttp().request<CartoonSeriesData>(
        'https://run.mocky.io/v3/86e1609f-7823-4c50-afdd-3e764642fb02',
        method: HttpConfig.get);
    CartoonSeriesData cartoonSeriesData = responseModel.data;
    return cartoonSeriesData.seriesComics;
  }

  /// 请求推荐数据
  getRecommendData() async {
    ResponseModel? responseModel = await LttHttp().request<CartoonRecommendData>(
        'https://run.mocky.io/v3/232c1f4b-4d84-423d-9438-6d9ddf5034be',
        method: HttpConfig.get);
    CartoonRecommendData cartoonRecommendData = responseModel.data;
    return cartoonRecommendData.infos;
  }
}

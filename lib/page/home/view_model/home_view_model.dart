import 'package:flutter/cupertino.dart';
import 'package:mvvm_provider/base/base_state.dart';
import 'package:mvvm_provider/model/banner_model.dart';
import 'package:mvvm_provider/page/home/states/home_state.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../base/base_view_model.dart';
import '../../../config/handle_state.dart';
import '../../../model/response_model.dart';
import '../../../net/ltt_https.dart';
import '../../../net/http_config.dart';
import '../../../widgets/easy_loading.dart';
import '../model/cartoon_model.dart';

class HomeViewModel extends BaseViewModel {
  /// 创建state
  HomeState homeState = HomeState();

  /// 获取列表数据
  Future<void> getListData(String url, int number) async {
    if (url == '') {
      /// 没有更多数据了
      refreshController.refreshCompleted();
      refreshController.loadComplete();
      refreshController.loadNoData();

      homeState.netState = NetState.dataSuccessState;
      notifyListeners();
      return;
    }
    ResponseModel? responseModel =
        await LttHttp().request<CarDataModel>(url, method: HttpConfig.get);
    homeState.netState = HandleState.handle(responseModel, successCode: 0);
    if (homeState.netState == NetState.dataSuccessState) {
      CarDataModel carDataModel = responseModel.data;
      if (number == 1) {
        homeState.dataList = carDataModel.feeds;
        if ((homeState.dataList ?? []).isEmpty) {
          homeState.netState = NetState.emptyDataState;
        }
      } else {
        homeState.dataList?.addAll(carDataModel.feeds ?? []);

        /// 显示没有更多数据了
        if ((carDataModel.feeds ?? []).isEmpty) {
          refreshController.loadNoData();
        }
      }
      refreshController.refreshCompleted();
      refreshController.loadComplete();
    }
    notifyListeners();
  }

  void changeIsLike() {
    homeState.isLike = !homeState.isLike;
    notifyListeners();
  }
}

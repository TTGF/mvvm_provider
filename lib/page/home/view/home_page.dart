import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mvvm_provider/base/multi_state_widget.dart';
import '../../../base/base_grid_view.dart';
import '../../../base/base_stateful_page.dart';
import '../../../base/provider_consumer_widget.dart';
import '../../../routers/home_router.dart';
import '../../../routers/navigator_utils.dart';
import '../view_model/home_view_model.dart';
import '../widgets/car_toon_widget.dart';
import '../widgets/test.dart';

class HomePage extends BasePage {
  const HomePage({super.key});

  @override
  BasePageState<BasePage> getState() => _HomePageState();
}

class _HomePageState extends BasePageState<HomePage> {
  HomeViewModel homeViewModel = HomeViewModel();
  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    super.pageTitle = '首页';
    isBack = false;

    _onRefresh();
  }

  @override
  Widget left() {
    return const SizedBox();
  }

  /// 请求分页
  int _pageNum = 1;

  /// 上拉加载
  void _onLoading() {
    _pageNum++;
    getListData();
  }

  /// 下拉刷新
  void _onRefresh() {
    _pageNum = 1;
    getListData();
  }

  void getListData() {
    homeViewModel.getListData(getUrl(_pageNum), _pageNum);
  }

  String getUrl(int number) {
    String urlStr = '';
    if (_pageNum == 1) {
      urlStr = 'https://run.mocky.io/v3/59e3cc71-f43f-4d44-b81d-0d97b701569e';
    } else if (_pageNum == 2) {
      urlStr = 'https://run.mocky.io/v3/bdb6ae2d-2ceb-41e6-8361-3713320dcb07';
    } else if (_pageNum == 3) {
      urlStr = 'https://run.mocky.io/v3/bcc02347-ec86-4d2f-b9c5-62acddbde05b';
    } else {
      urlStr = '';
    }
    return urlStr;
  }

  @override
  Widget buildPage(BuildContext context) {
    // return Container(
    //   color: Colors.white,
    //   child: TextButton(
    //     child: const Text('点击拍照'),
    //     onPressed: () async {
    //       //拍照
    //       XFile? file = await _picker.pickImage(source: ImageSource.camera,imageQuality: 100);
    //       NavigatorUtils.push(context, HomeRouter.waterMarkPage,
    //           arguments: {"imagePath": file!.path});
    //     },
    //   ),
    // );


    return ProviderConsumerWidget<HomeViewModel>(
      viewModel: homeViewModel,
      builder: (context, viewModel, child) {
        return MultiStateWidget(
            netState: homeViewModel.homeState.netState,
            placeHolderType: PlaceHolderType.gridViewPlaceHolder,
            builder: (BuildContext context) {
              return Container(
                color: Colors.deepOrange,
                child: BaseGridView(
                  enablePullDown: true,
                  enablePullUp: true,
                  onRefresh: _onRefresh,
                  onLoading: _onLoading,
                  refreshController: viewModel.refreshController,
                  scrollController: viewModel.scrollController,
                  data: viewModel.homeState.dataList ?? [],
                  padding: EdgeInsets.all(10.h),
                  childAspectRatio: 0.7,
                  crossAxisSpacing: 10.w,
                  mainAxisSpacing: 10.h,
                  crossAxisCount: 2,
                  bgColor: const Color(0xFFF3F4F8),
                  itemBuilder: (context22, index) {
                    return CarToonWidget(
                      index: index,
                      model: viewModel.homeState.dataList![index],
                      onTap: () async {
                        NavigatorUtils.push(context, HomeRouter.homeDetailPage,
                            arguments: {"imageUrl": homeViewModel.homeState.dataList?[index].image});
                      },
                    );
                  },
                ),
              );
            });
      },
    );
  }
}

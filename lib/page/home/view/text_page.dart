import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mvvm_provider/base/select_plus_data.dart';

import '../../../base/provider_selector_widget.dart';
import '../view_model/home_view_model.dart';

class TextPage extends StatefulWidget {
  const TextPage({super.key});

  @override
  State<TextPage> createState() => _TextPageState();
}

class _TextPageState extends State<TextPage> {
  HomeViewModel homeViewModel = HomeViewModel();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 200.h),
      child: Column(
        children: [
          ProviderSelectorWidget<HomeViewModel, bool>(
            viewModel: homeViewModel,
            builder: (context, isLike, child) {
              return Container(
                color: Colors.blue,
                child: Center(
                  child: Text('选择是 $isLike'),
                ),
              );
            },
            selector: (context, homeViewModel) => homeViewModel.homeState.isLike,
          ),
          TextButton(
              onPressed: () {
                homeViewModel.changeIsLike();
              },
              child: Container(
                alignment: Alignment.center,
                width: 100.h,
                height: 40.h,
                color: Colors.redAccent,
                child: const Text('点我'),
              )),
        ],
      ),
    );
  }
}

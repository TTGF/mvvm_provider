import 'dart:convert';

import '../../../generated/json/base/json_field.dart';
import '../../../generated/json/cartoon_series_entity.g.dart';

@JsonSerializable()
class CartoonSeriesData {
  @JSONField(name: "series_comics")
  late List<CartoonSeriesDataSeriesComics> seriesComics;

  CartoonSeriesData();

  factory CartoonSeriesData.fromJson(Map<String, dynamic> json) => $CartoonSeriesDataFromJson(json);

  Map<String, dynamic> toJson() => $CartoonSeriesDataToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class CartoonSeriesDataSeriesComics {
  late int id;
  late String title;
  @JSONField(name: "horizontal_cover")
  late String horizontalCover;
  @JSONField(name: "square_cover")
  late String squareCover;
  @JSONField(name: "vertical_cover")
  late String verticalCover;
  @JSONField(name: "is_finish")
  late int isFinish;
  late int status;
  @JSONField(name: "last_ord")
  late int lastOrd;
  late int total;
  @JSONField(name: "last_short_title")
  late String lastShortTitle;
  late List<String> authors;
  late List<CartoonSeriesDataSeriesComicsStyles> styles;
  late int type;

  CartoonSeriesDataSeriesComics();

  factory CartoonSeriesDataSeriesComics.fromJson(Map<String, dynamic> json) =>
      $CartoonSeriesDataSeriesComicsFromJson(json);

  Map<String, dynamic> toJson() => $CartoonSeriesDataSeriesComicsToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class CartoonSeriesDataSeriesComicsStyles {
  late int id;
  late String name;

  CartoonSeriesDataSeriesComicsStyles();

  factory CartoonSeriesDataSeriesComicsStyles.fromJson(Map<String, dynamic> json) =>
      $CartoonSeriesDataSeriesComicsStylesFromJson(json);

  Map<String, dynamic> toJson() => $CartoonSeriesDataSeriesComicsStylesToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

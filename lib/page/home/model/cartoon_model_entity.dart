import 'dart:convert';

import '../../../generated/json/base/json_field.dart';
import '../../../generated/json/cartoon_model_entity.g.dart';

@JsonSerializable()
class CartoonModelData {
	late int id;
	late String title;
	late CartoonModelDataAuthor author;
	late CartoonModelDataCover cover;
	@JSONField(name: "finish_status")
	late int finishStatus;
	late int number;
	@JSONField(name: "ep_total")
	late int epTotal;
	late CartoonModelDataStyle style;
	late List<CartoonModelDataTag> tag;
	late String evaluate;
	late List<CartoonModelDataEp> ep;
	@JSONField(name: "auto_pay_status")
	late int autoPayStatus;
	late CartoonModelDataCopyright copyright;
	late int favorite;
	late List<dynamic> discount;
	late int status;
	@JSONField(name: "comment_id")
	late String commentId;
	@JSONField(name: "comment_state")
	late int commentState;
	@JSONField(name: "comment_share_state")
	late int commentShareState;
	@JSONField(name: "discount_state")
	late int discountState;
	late List<CartoonModelDataStyles> styles;

	CartoonModelData();

	factory CartoonModelData.fromJson(Map<String, dynamic> json) => $CartoonModelDataFromJson(json);

	Map<String, dynamic> toJson() => $CartoonModelDataToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class CartoonModelDataAuthor {
	late int id;
	late String name;

	CartoonModelDataAuthor();

	factory CartoonModelDataAuthor.fromJson(Map<String, dynamic> json) => $CartoonModelDataAuthorFromJson(json);

	Map<String, dynamic> toJson() => $CartoonModelDataAuthorToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class CartoonModelDataCover {
	@JSONField(name: "v_cover")
	late String vCover;
	@JSONField(name: "s_cover")
	late String sCover;

	CartoonModelDataCover();

	factory CartoonModelDataCover.fromJson(Map<String, dynamic> json) => $CartoonModelDataCoverFromJson(json);

	Map<String, dynamic> toJson() => $CartoonModelDataCoverToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class CartoonModelDataStyle {
	late int id;
	late String name;
	@JSONField(name: "p_id")
	late int pId;

	CartoonModelDataStyle();

	factory CartoonModelDataStyle.fromJson(Map<String, dynamic> json) => $CartoonModelDataStyleFromJson(json);

	Map<String, dynamic> toJson() => $CartoonModelDataStyleToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class CartoonModelDataTag {
	late int id;
	late String name;

	CartoonModelDataTag();

	factory CartoonModelDataTag.fromJson(Map<String, dynamic> json) => $CartoonModelDataTagFromJson(json);

	Map<String, dynamic> toJson() => $CartoonModelDataTagToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class CartoonModelDataEp {
	late int id;
	late String title;
	@JSONField(name: "short_title")
	late String shortTitle;
	@JSONField(name: "pay_mode")
	late int payMode;
	@JSONField(name: "is_locked")
	late bool isLocked;
	@JSONField(name: "unlock_type")
	late int unlockType;
	late int read;
	@JSONField(name: "last_read")
	late int lastRead;
	@JSONField(name: "first_online_time")
	late String firstOnlineTime;

	CartoonModelDataEp();

	factory CartoonModelDataEp.fromJson(Map<String, dynamic> json) => $CartoonModelDataEpFromJson(json);

	Map<String, dynamic> toJson() => $CartoonModelDataEpToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class CartoonModelDataCopyright {
	late int limit;

	CartoonModelDataCopyright();

	factory CartoonModelDataCopyright.fromJson(Map<String, dynamic> json) => $CartoonModelDataCopyrightFromJson(json);

	Map<String, dynamic> toJson() => $CartoonModelDataCopyrightToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class CartoonModelDataStyles {
	late int id;
	late String name;
	@JSONField(name: "p_id")
	late int pId;

	CartoonModelDataStyles();

	factory CartoonModelDataStyles.fromJson(Map<String, dynamic> json) => $CartoonModelDataStylesFromJson(json);

	Map<String, dynamic> toJson() => $CartoonModelDataStylesToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}
import 'dart:convert';

import '../../../generated/json/base/json_field.dart';
import '../../../generated/json/cartoon_model.g.dart';

@JsonSerializable()
class CarDataModel {
  List<CartoonModel>? feeds;
  int? show_times;
  CarDataModel();

  factory CarDataModel.fromJson(Map<String, dynamic> json) => $CarDataModelFromJson(json);

  Map<String, dynamic> toJson() => $CarDataModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class CartoonModel {
  String? item_id;
  String? parent_id;
  String? title;
  String? jump_value;
  int? type;
  String? image;
  int crossAxisCount = 1;
  int mainAxisCount = 1;
  CommicInfoModel? comic_info;
  CartoonModel();

  factory CartoonModel.fromJson(Map<String, dynamic> json) => $CartoonModelFromJson(json);

  Map<String, dynamic> toJson() => $CartoonModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

@JsonSerializable()
class CommicInfoModel {
  String? decision;
  String? lastest_short_title;
  String? title;
  int? is_finish;
  String? main_style_name;
  double? score;
  List<String>? styles;
  List<String>? tags;
  CommicInfoModel();

  factory CommicInfoModel.fromJson(Map<String, dynamic> json) => $CommicInfoModelFromJson(json);

  Map<String, dynamic> toJson() => $CommicInfoModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

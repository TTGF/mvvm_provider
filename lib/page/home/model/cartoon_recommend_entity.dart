import 'dart:convert';

import '../../../generated/json/base/json_field.dart';
import '../../../generated/json/cartoon_recommend_entity.g.dart';

@JsonSerializable()
class CartoonRecommendData {
	late List<CartoonRecommendDataInfos> infos;

	CartoonRecommendData();

	factory CartoonRecommendData.fromJson(Map<String, dynamic> json) => $CartoonRecommendDataFromJson(json);

	Map<String, dynamic> toJson() => $CartoonRecommendDataToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class CartoonRecommendDataInfos {
	late int id;
	late String title;
	late CartoonRecommendDataInfosAuthor author;
	late CartoonRecommendDataInfosCover cover;
	@JSONField(name: "same_author")
	late int sameAuthor;
	@JSONField(name: "finish_status")
	late int finishStatus;
	@JSONField(name: "ep_total")
	late int epTotal;
	late CartoonRecommendDataInfosStyle style;
	late List<CartoonRecommendDataInfosTag> tag;

	CartoonRecommendDataInfos();

	factory CartoonRecommendDataInfos.fromJson(Map<String, dynamic> json) => $CartoonRecommendDataInfosFromJson(json);

	Map<String, dynamic> toJson() => $CartoonRecommendDataInfosToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class CartoonRecommendDataInfosAuthor {
	late int id;
	late String name;

	CartoonRecommendDataInfosAuthor();

	factory CartoonRecommendDataInfosAuthor.fromJson(Map<String, dynamic> json) => $CartoonRecommendDataInfosAuthorFromJson(json);

	Map<String, dynamic> toJson() => $CartoonRecommendDataInfosAuthorToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class CartoonRecommendDataInfosCover {
	@JSONField(name: "v_cover")
	late String vCover;
	@JSONField(name: "s_cover")
	late String sCover;

	CartoonRecommendDataInfosCover();

	factory CartoonRecommendDataInfosCover.fromJson(Map<String, dynamic> json) => $CartoonRecommendDataInfosCoverFromJson(json);

	Map<String, dynamic> toJson() => $CartoonRecommendDataInfosCoverToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class CartoonRecommendDataInfosStyle {
	late int id;
	late String name;
	@JSONField(name: "p_id")
	late int pId;

	CartoonRecommendDataInfosStyle();

	factory CartoonRecommendDataInfosStyle.fromJson(Map<String, dynamic> json) => $CartoonRecommendDataInfosStyleFromJson(json);

	Map<String, dynamic> toJson() => $CartoonRecommendDataInfosStyleToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class CartoonRecommendDataInfosTag {
	late int id;
	late String name;

	CartoonRecommendDataInfosTag();

	factory CartoonRecommendDataInfosTag.fromJson(Map<String, dynamic> json) => $CartoonRecommendDataInfosTagFromJson(json);

	Map<String, dynamic> toJson() => $CartoonRecommendDataInfosTagToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../widgets/app_net_image.dart';
import '../model/cartoon_recommend_entity.dart';

class RecommendWidget extends StatelessWidget {
  final List<CartoonRecommendDataInfos> list;

  const RecommendWidget({super.key, required this.list});

  @override
  Widget build(BuildContext context) {

    Widget item(int index) {
      return Column(
        children: [
          AppNetImage(
            imageUrl: list[index].cover.vCover,
            width: (1.sw - 60.w) / 4,
            height: 100.h,
            fit: BoxFit.cover,
            radius: 4.h,
          ),
          Container(
            width: (1.sw - 60.w) / 4,
            padding: EdgeInsets.only(top: 6.h),
            alignment: Alignment.centerLeft,
            child: Text(
              list[index].title,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Colors.black,
                fontSize: 12.sp,
              ),
            ),
          ),
          Container(
            width: 1.sw - 140.w,
            height: 25.h,
            alignment: Alignment.centerLeft,
            child: Text(
              list[index].author.name,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Colors.black54,
                fontSize: 11.sp,
              ),
            ),
          ),
        ],
      );
    }

    return Container(
      height: 360.h,
      width: 1.sw,
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 15.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('更多推荐',
              style: TextStyle(
                  color: const Color(0xFF000000), fontSize: 15.sp, fontWeight: FontWeight.w500)),
          SizedBox(
            height: 15.h,
          ),
          Expanded(
              child: GridView.builder(
            itemCount: list.length,
            physics: const NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 0.52, //宽高比
                crossAxisSpacing: 10.w, //水平间距
                mainAxisSpacing: 10.h, //垂直间距
                crossAxisCount: 4),
            itemBuilder: (context, index) {
              return item(index);
            },
          ))
        ],
      ),
    );
  }
}

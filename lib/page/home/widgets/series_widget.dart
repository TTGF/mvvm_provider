import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../widgets/app_net_image.dart';
import '../model/cartoon_series_entity.dart';

class SeriesWidget extends StatelessWidget {
  final List<CartoonSeriesDataSeriesComics> seriesList;
  const SeriesWidget({super.key, required this.seriesList});

  @override
  Widget build(BuildContext context) {
    CartoonSeriesDataSeriesComics model = seriesList[0];
    return Container(
      padding: EdgeInsets.only(left: 15.w, right: 15.w, bottom: 24.h),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('系列作品',
              style: TextStyle(
                  color: const Color(0xFF000000), fontSize: 15.sp, fontWeight: FontWeight.w500)),
          SizedBox(
            height: 15.h,
          ),
          Row(
            children: [
              AppNetImage(
                imageUrl: model.verticalCover,
                width: 90.w,
                height: 110.w,
                fit: BoxFit.cover,
                radius: 4.h,
              ),
              SizedBox(
                width: 10.w,
              ),
              Column(
                children: [
                  SizedBox(
                    width: 1.sw - 140.w,
                    child: Text(
                      model.title ?? '',
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 13.sp,
                      ),
                    ),
                  ),
                  Container(
                    width: 1.sw - 140.w,
                    height: 25.h,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      '更新至${model.lastOrd} 话',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 12.sp,
                      ),
                    ),
                  ),
                  Container(
                    width: 1.sw - 140.w,
                    height: 25.h,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      model.styles[0].name ?? '',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 12.sp,
                      ),
                    ),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}

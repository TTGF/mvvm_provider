import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../widgets/app_net_image.dart';
import '../model/cartoon_model_entity.dart';

class NovelHeaderWidget extends StatelessWidget {
  final int? type;
  final CartoonModelData model;
  final String imageUrl;
  const NovelHeaderWidget({super.key, required this.model, required this.imageUrl, this.type});

  /// 遮罩
  Widget maskWidget() {
    return Center(
      child: ClipRect(
        //背景过滤器
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 40.0, sigmaY: 40.0),
          child: Opacity(
            opacity: 0.6,
            child: Container(
              width: 1.sw,
              height: 280.h,
              decoration: const BoxDecoration(color: Colors.black54),
            ),
          ),
        ),
      ),
    );
  }

  Widget contentWidget() {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppNetImage(
              imageUrl: imageUrl,
              width: 120.w,
              height: 150.w,
              fit: BoxFit.fitWidth,
              radius: 8.h,
            ),
            SizedBox(
              width: 10.w,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 1.sw - 170.w,
                  child: Text(
                    type == null ? model.title ?? '' : '一个bloc完成局部刷新',
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.sp,
                    ),
                  ),
                ),
                Container(
                  width: 1.sw - 170.w,
                  height: 40.h,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    '${model.author.name ?? ''} 著',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12.sp,
                    ),
                  ),
                ),
                Container(
                  width: 1.sw - 170.w,
                  height: 40.h,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    '连载中·8.28更新至第155章|34万字',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white38,
                      fontSize: 12.sp,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
        Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(left: (1.sw + 160.w) / 2),
          width: 80.w,
          height: 30.h,
          decoration: BoxDecoration(
              color: Colors.orange,
              border: Border.all(width: 1.w, color: Colors.orange),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(6.h), bottomRight: Radius.circular(6.h))),
          child: Text(
            '追书',
            style: TextStyle(color: Colors.white, fontSize: 15.sp, fontWeight: FontWeight.w500),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AppNetImage(
          imageUrl: imageUrl,
          width: 1.sw,
          height: 280.h,
          fit: BoxFit.fitWidth,
        ),
        maskWidget(),
        Container(
          height: 280.h,
          width: 1.sw,
          padding: EdgeInsets.only(
              left: 15.w, right: 15.w, top: kToolbarHeight + ScreenUtil().statusBarHeight),
          child: contentWidget(),
        )
      ],
    );
  }
}

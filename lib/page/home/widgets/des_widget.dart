import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../model/cartoon_model_entity.dart';

class DesWidget extends StatelessWidget {
  final CartoonModelData model;

  const DesWidget({super.key, required this.model});

  /// tag文字
  Widget tagText(String titleStr) {
    return Container(
      margin: EdgeInsets.only(left: 15.w, top: 10.h, bottom: 10.h),
      padding: EdgeInsets.symmetric(horizontal: 10.h, vertical: 5.h),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4.0),
        color: const Color(0xFFF6F9FF),
      ),
      child: Text(
        titleStr,
        style: TextStyle(fontSize: 12.sp, color: const Color(0xFF666666)),
      ),
    );
  }

  /// tag
  Widget tagsWidget() {
    List<Widget> titleWidget = [];
    titleWidget.add(tagText(model.style.name));
    for (int i = 0; i < model.tag.length; i++) {
      CartoonModelDataTag cartoonModelDataTag = model.tag[i];
      titleWidget.add(tagText(cartoonModelDataTag.name));
    }
    return Row(
      children: titleWidget,
    );
  }

  /// 描述内容
  Widget textWidget() {
    return Container(
      padding: EdgeInsets.only(left: 15.w, right: 15.w, bottom: 15.h),
      color: Colors.white,
      child: Text(
        model.evaluate,
        style: TextStyle(color: const Color(0xFF666666), fontSize: 12.sp, height: 2.h),
      ),
    );
  }

  /// 小说评论
  Widget commitWidget() {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(left: 15.w, right: 15.h, bottom: 30.h),
      padding: EdgeInsets.symmetric(
        horizontal: 10.h,
        vertical: 10.h,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4.0),
        border: Border.all(width: 1, color: const Color(0xFFF3F5FA)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('小说讨论区',
              style: TextStyle(
                  color: const Color(0xFF000000), fontSize: 14.sp, fontWeight: FontWeight.w500)),
          Text('2202条评论等你来 >',
              style: TextStyle(
                  color: const Color(0xFF666666), fontSize: 10.sp,)),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [tagsWidget(), textWidget(), commitWidget()],
      ),
    );
  }
}

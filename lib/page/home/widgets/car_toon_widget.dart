import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../widgets/app_net_image.dart';
import '../model/cartoon_model.dart';

class CarToonWidget extends StatelessWidget {
  final int index;
  final CartoonModel model;
  final bool? isStaggeredGrid;
  final Function onTap;

  const CarToonWidget({super.key, required this.index, required this.model, this.isStaggeredGrid,required this.onTap});

  /// 图片
  Widget imageWidget() {
    return AppNetImage(
      radius: 6.h,
      fit: BoxFit.fill,
      width: (1.sw - 30.w) / 2,
      height: 260.h,
      imageUrl: model.image ?? '',
    );
  }

  /// 标题
  Widget titleWidget() {
    return Container(
      margin: EdgeInsets.only(left: 10.w, right: 10.w, top: 10.h, bottom: 10.h),
      width: (1.sw - 30.w) / 2,
      height: 20.h,
      child: Text(
        model.title ?? '',
        overflow: TextOverflow.ellipsis,
        style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500, fontSize: 16.sp),
      ),
    );
  }

  /// 描述
  Widget desWidget() {
    String textStr = '';
    if ((model.comic_info!.tags ?? []).isNotEmpty) {
      if (model.comic_info!.main_style_name!.isNotEmpty) {
        textStr = "${model.comic_info?.main_style_name} | ${model.comic_info?.tags![0]}";
      } else {
        textStr = "${model.comic_info?.tags![0]}";
      }
    } else {
      if (model.comic_info!.main_style_name!.isEmpty) {
        textStr = "暂无描述";
      } else {
        textStr = model.comic_info?.main_style_name ?? "";
      }
    }
    return Container(
      margin: EdgeInsets.only(
        left: 10.w,
        right: 10.w,
      ),
      width: (1.sw - 30.w) / 2,
      height: 20.h,
      child: Text(
        textStr,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(color: Colors.black45, fontWeight: FontWeight.w500, fontSize: 14.sp),
      ),
    );
  }

  /// 上面视图
  Widget otherWidget() {
    return Container(
      width: (1.sw - 30.w) / 2,
      height: 80.h,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 1.w, color: Colors.black12),
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(6.h), bottomRight: Radius.circular(6.h))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          titleWidget(),
          desWidget(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap();
      },
      child: Stack(
        children: [
          Container(
              padding: isStaggeredGrid == true
                  ? EdgeInsets.only(
                      left: index % 3 == 1 ? 10.w : 5.w, right: index % 3 == 2 ? 10.w : 5.w)
                  : const EdgeInsets.all(0),
              decoration: BoxDecoration(
                color: const Color(0xFFF3F4F8),
                borderRadius: BorderRadius.all(Radius.circular(6.h)),
              ),
              alignment: Alignment.center,
              child: Stack(
                children: [imageWidget(), Positioned(bottom: 0, left: 0, child: otherWidget())],
              )),
        ],
      ),
    );
  }
}

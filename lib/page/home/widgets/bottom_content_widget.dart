import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../widgets/delay_button.dart';
import '../model/cartoon_model_entity.dart';

class BottomContentWidget extends StatefulWidget {
  final List<CartoonModelDataEp> epList;
  const BottomContentWidget({super.key, required this.epList});

  @override
  State<BottomContentWidget> createState() => _BottomContentWidgetState();
}

class _BottomContentWidgetState extends State<BottomContentWidget> {
  bool isStop = true;

  @override
  void initState() {
    super.initState();
  }

  /// 目录
  Widget catalogWidget() {
    return Positioned(
        left: 0,
        child: DelayButton(
          width: 56.w,
          decoration: const BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.black26, //底色,阴影颜色
                  offset: Offset(0.0, 0.0), //阴影xy轴偏移量
                  blurRadius: 6.0, //阴影模糊程度
                  spreadRadius: 0.1 //阴影扩散程度
                  ) //阴影模糊大小
            ],
          ),
          height: 56.h,
          onTap: () {},
          mainWidget: Align(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 2.h,
                ),
                Image.asset("assets/images/danmuliang_Normal@2x.png",
                    fit: BoxFit.cover, color: Colors.black45, width: 20.w, height: 15.w),
                SizedBox(
                  height: 2.h,
                ),
                Text(
                  '目录',
                  style: TextStyle(
                      color: Colors.black54, fontSize: 10.sp, fontWeight: FontWeight.w500),
                ),
              ],
            ),
          ),
        ));
  }

  /// 阅读
  Widget readWidget() {
    return Positioned(
        right: 0,
        child: AnimatedSize(
          duration: const Duration(milliseconds: 100),
          child: DelayButton(
            alignment: Alignment.center,
            decoration: const BoxDecoration(color: Colors.blue),
            width: isStop == true ? 80.w : 50.w,
            height: 56.h,
            onTap: () {},
            mainWidget: Text(
              isStop == true ? '继续阅读' : '续读',
              style: TextStyle(color: Colors.white, fontSize: 14.sp),
            ),
          ),
        ));
  }

  /// item
  Widget item(String titleStr) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(left: 10.w, top: 5.h, bottom: 5.h),
      height: 56.h,
      decoration:
          BoxDecoration(borderRadius: BorderRadius.circular(4.h), color: const Color(0xFFF3F5FA)),
      padding: EdgeInsets.symmetric(horizontal: 14.h),
      child: Text(
        titleStr,
        style: TextStyle(fontSize: 14.sp, color: Colors.black45),
      ),
    );
  }

  /// 章节list
  Widget sectionWidget() {
    return NotificationListener(
      onNotification: (ScrollNotification notification) {
        if (notification is ScrollStartNotification) {
          setState(() {
            isStop = false;
          });
        } else if (notification is ScrollUpdateNotification) {
        } else if (notification is ScrollEndNotification) {
          setState(() {
            isStop = true;
          });
        }
        return false;
      },
      child: SizedBox(
        width: 1.sw,
        height: 56.h,
        child: ListView.builder(
          itemCount: widget.epList.length,
          padding: EdgeInsets.only(left: 50.w, right: 80.w),
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            return item(widget.epList[index].shortTitle);
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 1.sw,
      height: 56.h,
      decoration: const BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.black26, //底色,阴影颜色
              offset: Offset(0.0, 0.0), //阴影xy轴偏移量
              blurRadius: 12.0, //阴影模糊程度
              spreadRadius: 0.4 //阴影扩散程度
              ) //阴影模糊大小
        ],
      ),
      child: Stack(
        children: [sectionWidget(), catalogWidget(), readWidget()],
      ),
    );
  }
}

import 'dart:convert';

import '../generated/json/banner_model.g.dart';
import '../generated/json/base/json_field.dart';



@JsonSerializable()
class BannerModel {
  int? id;
  String? title;
  String? img;
  int? jump_type;
  String? jump_value;
  String? img2;
  String? bg;
  BannerModel();

  factory BannerModel.fromJson(Map<String, dynamic> json) => $BannerModelFromJson(json);

  Map<String, dynamic> toJson() => $BannerModelToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}

import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import '../page/bookshelf/view/bookshelf_detail_page.dart';
import '../page/home/view/home_detail_page.dart';
import '../page/home/view/text_page.dart';
import '../page/mine/view/mine_detail_page.dart';
import '../page/mine/view/mine_detail_type2_page.dart';
import '../page/novel/view/novel_detail_page.dart';
import '../page/tabbar/view/tabber_page.dart';
import 'irouter_provider.dart';

class MineRouter extends IRouterProvider {
  static const mineDetailPage = "/pages/mine/view/mine_detail_page"; //资讯详情
  static const mineDetailType2Page = "/pages/mine/view/mine_detail_type2_page"; //资讯详情2
  static const novelDetailPage = "/page/novel/view/novel_detail_page"; //小说详情

  @override
  void initRouter(FluroRouter router) {
    router.define(mineDetailPage, handler: Handler(handlerFunc: (context, params) {
      Map? argument = context!.settings!.arguments as Map?;
      int entityId = argument?['entityId'] ?? 0;
      return MineDetailPage(
        entityId: entityId,
      );
    }));

    router.define(mineDetailType2Page, handler: Handler(handlerFunc: (context, params) {
      Map? argument = context!.settings!.arguments as Map?;
      int entityId = argument?['entityId'] ?? 0;
      return MineDetailType2Page(
        entityId: entityId,
      );
    }));

    router.define(novelDetailPage, handler: Handler(handlerFunc: (context, params) {
      return const NovelDetailPage();
    }));
  }
}

import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:mvvm_provider/page/home/widgets/test.dart';
import '../page/bookshelf/view/bookshelf_detail_page.dart';
import '../page/home/view/home_detail_page.dart';
import '../page/home/view/text_page.dart';
import '../page/tabbar/view/tabber_page.dart';
import 'irouter_provider.dart';

class HomeRouter extends IRouterProvider {
  static const messageDetailPage = "/pages/home/message_module/InformationDetailPage"; //资讯详情
  static const homeDetailPage = "/pages/home/novel_detail/homeDetailPage"; //首页详情
  static const bookShelfDetailPage = "/pages/home/bookshelf/bookShelfDetailPage"; //书架详情

  static const biliTabarPage = "/pages/home/tabbar/BiliTabarPage"; //小说详情
  static const tabberPage = "/pages/home/tabbar/TabberPage"; //小说详情

  static const testPage = "/pages/home/tabbar/TextPage"; //小说详情


  static const waterMarkPage = "/pages/home/tabbar/WaterMarkPage"; //小说详情


  @override
  void initRouter(FluroRouter router) {
    /// 资讯详情
    router.define(messageDetailPage, handler: Handler(handlerFunc: (context, params) {
      Map? argument = context!.settings!.arguments as Map?;
      int entityId = argument?['entityId'] ?? 0;
      return const TextPage();
    }));

    /// 资讯详情
    router.define(testPage, handler: Handler(handlerFunc: (context, params) {
      return TextPage();
    }));

    /// 首页详情
    router.define(homeDetailPage, handler: Handler(handlerFunc: (context, params) {
      Map? argument = context!.settings!.arguments as Map?;
      String imageUrl = argument?['imageUrl'];
      return HomeDetailPage(
        imageUrl: imageUrl,
      );
    }));

    /// 书架详情
    router.define(bookShelfDetailPage, handler: Handler(handlerFunc: (context, params) {
      Map? argument = context!.settings!.arguments as Map?;
      String imageUrl = argument?['imageUrl'];
      return BookShelfDetailPage(
        imageUrl: imageUrl,
      );
    }));


    /// 书架详情
    router.define(waterMarkPage, handler: Handler(handlerFunc: (context, params) {
      Map? argument = context!.settings!.arguments as Map?;
      String imagePath = argument?['imagePath'];
      return WaterMarkPage(
        imagePath: imagePath,
      );
    }));
  }
}

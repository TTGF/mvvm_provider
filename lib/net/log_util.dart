import 'package:logger/logger.dart';
import 'dart:developer';

/// FileName: log_util
///
/// Description:
/// Author: 朱亚朋
/// Date: 2021/11/17

///默认tag
const String _tag = "!@!@!@--->>>";

Logger _logger = Logger(
  printer: PrettyPrinter(
    methodCount: 0,
  ),
);

logV(dynamic msg, {String tag = _tag}) {
  _logger.v("$msg");
}

logD(dynamic msg, {String tag = _tag}) {
  _logger.d("$msg");
}

logI(dynamic msg, {String tag = _tag}) {
  _logger.i("$msg");
}

logW(dynamic msg, {String tag = _tag}) {
  _logger.w("$msg");
}

logE(dynamic msg, {String tag = _tag}) {
  _logger.e("$msg");
}

logWTF(dynamic msg, {String tag = _tag}) {;
  log(msg);
}

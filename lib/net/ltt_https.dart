import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:uuid/uuid.dart';
import '../model/response_model.dart';
import '../widgets/easy_loading.dart';
import 'http_config.dart';
import 'network_interceptors.dart';

class LttHttp {
  Dio? _dio;
  // final cancelToken = CancelToken();

  /// 用于拼装tracId
  Uuid? uuid;
  String traceId = Platform.isAndroid ? 'Android-overseas' : 'ios-overseas';

  /// 私有构造
  LttHttp._();

  /// 实例化Http
  factory LttHttp() => _instance;

  /// 初始化配置
  static final LttHttp _instance = LttHttp._internal();

  /// 自定义Header
  final httpHeaders = {
    'Accept': 'application/json,*/*',
    'Content-Type': 'application/json',
    'api-version': "",
    'app-version': "",
    'app-platform': Platform.isAndroid ? 'Android' : 'iOS',
    'traceId': Platform.isAndroid ? 'Android-server' : 'ios-server',
    'platform': 'OWNER',
  };

  /// 通用全局单例，第一次使用时初始化
  LttHttp._internal() {
    uuid ??= const Uuid();
    if (_dio == null) {
      uuid ??= const Uuid();
      // 初始化dio
      _dio = Dio(_options());
      // 拦截器
      _dio?.interceptors.add(_interceptors());
      _dio?.interceptors.add(LogsInterceptors());
    }
  }

  /// 获取拦截器
  InterceptorsWrapper _interceptors() {
    return InterceptorsWrapper(
      onRequest: (RequestOptions options, RequestInterceptorHandler handler) {
        appendCommonHeaders(options.headers);
        return handler.next(options);
      },
      onResponse: (Response response, ResponseInterceptorHandler handler) {
        return handler.next(response);
      },
      onError: (DioError e, ErrorInterceptorHandler handler) {
        return handler.next(e);
      },
    );
  }

  /// 获取基础配置项
  BaseOptions _options() {
    // 获取当前环境对应的配置项
    String baseUrl = "https://api.chexun.com/";
    return BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: HttpConfig.connectTimeout,
      receiveTimeout: HttpConfig.receiveTimeout,
      headers: httpHeaders,
    );
  }

  /// 给每一个请求设置唯一id
  void _setTraceId(String url) {
    if (_dio != null && uuid != null) {
      String uid = uuid!.v4();
      _dio?.options.headers['traceId'] = '$traceId-$url-$uid';
    }
  }

  /// get请求
  ///
  /// [path] 必传，请求路径
  /// [params] 非必传，请求入参
  Future<ResponseModel> _get<T>(
    String path, {
    Map<String, dynamic>? params,
  }) async {
    _setTraceId(path);
    params = params ?? {};
    ResponseModel? responseModel;
    try {
      Response? response = await _dio?.get(path, queryParameters: params);
      var responseData = response?.data;
      responseModel = ResponseModel<T>.fromJson(responseData);
      _handleStatusCode(responseModel);
    } on DioError catch (dioError, e) {
      _handleError(dioError, e);
      if (dioError.requestOptions.cancelToken is CancelToken) {
        responseModel = ResponseModel(data: null, code: -1, message: '取消请求');
      } else {
        if (dioError.response!.data is Map) {
          responseModel =
              ResponseModel(data: null, code: dioError.response!.data['code'], message: '网络异常');
        } else {
          responseModel = ResponseModel(data: null, code: 404, message: '网络异常');
        }
      }
      if (dioError is TimeoutException) {
        responseModel = ResponseModel(data: null, code: -100, message: '网络超时');
      }
    }
    return responseModel;
  }

  static ResponseModel decode<T>(dynamic json) {
    return ResponseModel<T>.fromJson(json);
  }

  /// post请求
  ///
  /// [path] 必传，请求路径
  /// [params] 非必传，请求入参
  Future<ResponseModel> _post<T>(
    String path, {
    Map<String, dynamic>? params,
  }) async {
    ResponseModel? responseModel;
    print(params);
    try {
      Response? response = await _dio?.post(path, data: params);
      var responseData = response?.data;
      responseModel = ResponseModel<T>.fromJson(responseData);
      _handleStatusCode(responseModel);
    } on DioError catch (dioError, e) {
      _handleError(dioError, e);
      responseModel =
          ResponseModel(data: null, code: dioError.response!.data['code'], message: '网络异常');
      if (dioError is TimeoutException) {
        responseModel = ResponseModel(data: null, code: -100, message: '网络超时');
      }
    }
    return responseModel;
  }

  /// mack数据
  ///
  /// [path] 必传|接口路径
  /// [params] 选填|接口入参
  Future<ResponseModel> _mock<T>(
    String path, {
    Map<String, dynamic>? params,
  }) async {
    ResponseModel? responseModel;
    print(params);
    try {
      var responseStr = await rootBundle.loadString(path);
      var response = json.decode(responseStr);
      var responseData = response;
      responseModel = ResponseModel<T>.fromJson(responseData);
      _handleStatusCode(responseModel);
    } catch (e) {
      responseModel = ResponseModel(data: null, code: -200, message: 'mack没有数据了');
    }
    return responseModel;
  }

  /// 发起请求
  /// [path] 请求链接
  /// [params] 请求参数
  /// [method] 请求方式
  Future<ResponseModel> request<T>(
    String path, {
    Map<String, dynamic>? params,
    String method = HttpConfig.post,
  }) async {
    _setTraceId(path);
    params = params ?? {};
    if (method == HttpConfig.get) {
      return await _get<T>(path, params: params);
    } else if (method == HttpConfig.post) {
      return await _post<T>(path, params: params);
    } else if (method == HttpConfig.mock) {
      return await _mock<T>(path, params: params);
    } else {
      throw HttpException('request method $method is not support');
    }
  }

  void cancelRequest() {
    // cancelToken.cancel();
  }

  /// 状态码处理
  void _handleStatusCode(ResponseModel? responseModel) {
    if (responseModel?.code == null) return;
    switch (responseModel?.code) {
      /// token失效
      case 1001:

        /// 退出登录
        break;
      default:
        break;
    }
  }

  /// dio异常处理
  void _handleError(DioError error, e) {
    debugPrintStack(
        stackTrace: error.stackTrace, label: '--------------- HTTP ERROR ----------------');
    XsEasyLoading.showToast('网络异常');
  }

  /// 往header添加动态公参
  void appendCommonHeaders(Map<String, dynamic> headers) {
    headers['token'] = 'Account().token';
    headers['language'] = 'SFLocaleConfig.currentLanguageCode()';
  }
}

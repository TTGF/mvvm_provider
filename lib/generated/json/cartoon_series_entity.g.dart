import 'package:mvvm_provider/generated/json/base/json_convert_content.dart';
import 'package:mvvm_provider/page/home/model/cartoon_series_entity.dart';

CartoonSeriesData $CartoonSeriesDataFromJson(Map<String, dynamic> json) {
	final CartoonSeriesData cartoonSeriesData = CartoonSeriesData();
	final List<CartoonSeriesDataSeriesComics>? seriesComics = jsonConvert.convertListNotNull<CartoonSeriesDataSeriesComics>(json['series_comics']);
	if (seriesComics != null) {
		cartoonSeriesData.seriesComics = seriesComics;
	}
	return cartoonSeriesData;
}

Map<String, dynamic> $CartoonSeriesDataToJson(CartoonSeriesData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['series_comics'] =  entity.seriesComics.map((v) => v.toJson()).toList();
	return data;
}

CartoonSeriesDataSeriesComics $CartoonSeriesDataSeriesComicsFromJson(Map<String, dynamic> json) {
	final CartoonSeriesDataSeriesComics cartoonSeriesDataSeriesComics = CartoonSeriesDataSeriesComics();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		cartoonSeriesDataSeriesComics.id = id;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		cartoonSeriesDataSeriesComics.title = title;
	}
	final String? horizontalCover = jsonConvert.convert<String>(json['horizontal_cover']);
	if (horizontalCover != null) {
		cartoonSeriesDataSeriesComics.horizontalCover = horizontalCover;
	}
	final String? squareCover = jsonConvert.convert<String>(json['square_cover']);
	if (squareCover != null) {
		cartoonSeriesDataSeriesComics.squareCover = squareCover;
	}
	final String? verticalCover = jsonConvert.convert<String>(json['vertical_cover']);
	if (verticalCover != null) {
		cartoonSeriesDataSeriesComics.verticalCover = verticalCover;
	}
	final int? isFinish = jsonConvert.convert<int>(json['is_finish']);
	if (isFinish != null) {
		cartoonSeriesDataSeriesComics.isFinish = isFinish;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		cartoonSeriesDataSeriesComics.status = status;
	}
	final int? lastOrd = jsonConvert.convert<int>(json['last_ord']);
	if (lastOrd != null) {
		cartoonSeriesDataSeriesComics.lastOrd = lastOrd;
	}
	final int? total = jsonConvert.convert<int>(json['total']);
	if (total != null) {
		cartoonSeriesDataSeriesComics.total = total;
	}
	final String? lastShortTitle = jsonConvert.convert<String>(json['last_short_title']);
	if (lastShortTitle != null) {
		cartoonSeriesDataSeriesComics.lastShortTitle = lastShortTitle;
	}
	final List<String>? authors = jsonConvert.convertListNotNull<String>(json['authors']);
	if (authors != null) {
		cartoonSeriesDataSeriesComics.authors = authors;
	}
	final List<CartoonSeriesDataSeriesComicsStyles>? styles = jsonConvert.convertListNotNull<CartoonSeriesDataSeriesComicsStyles>(json['styles']);
	if (styles != null) {
		cartoonSeriesDataSeriesComics.styles = styles;
	}
	final int? type = jsonConvert.convert<int>(json['type']);
	if (type != null) {
		cartoonSeriesDataSeriesComics.type = type;
	}
	return cartoonSeriesDataSeriesComics;
}

Map<String, dynamic> $CartoonSeriesDataSeriesComicsToJson(CartoonSeriesDataSeriesComics entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['title'] = entity.title;
	data['horizontal_cover'] = entity.horizontalCover;
	data['square_cover'] = entity.squareCover;
	data['vertical_cover'] = entity.verticalCover;
	data['is_finish'] = entity.isFinish;
	data['status'] = entity.status;
	data['last_ord'] = entity.lastOrd;
	data['total'] = entity.total;
	data['last_short_title'] = entity.lastShortTitle;
	data['authors'] =  entity.authors;
	data['styles'] =  entity.styles.map((v) => v.toJson()).toList();
	data['type'] = entity.type;
	return data;
}

CartoonSeriesDataSeriesComicsStyles $CartoonSeriesDataSeriesComicsStylesFromJson(Map<String, dynamic> json) {
	final CartoonSeriesDataSeriesComicsStyles cartoonSeriesDataSeriesComicsStyles = CartoonSeriesDataSeriesComicsStyles();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		cartoonSeriesDataSeriesComicsStyles.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		cartoonSeriesDataSeriesComicsStyles.name = name;
	}
	return cartoonSeriesDataSeriesComicsStyles;
}

Map<String, dynamic> $CartoonSeriesDataSeriesComicsStylesToJson(CartoonSeriesDataSeriesComicsStyles entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}
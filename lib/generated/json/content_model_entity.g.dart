import 'package:mvvm_provider/generated/json/base/json_convert_content.dart';
import 'package:mvvm_provider/page/novel/model/content_model_entity.dart';

ContentModelData $ContentModelDataFromJson(Map<String, dynamic> json) {
	final ContentModelData contentModelData = ContentModelData();
	final List<ContentModelDataList>? list = jsonConvert.convertListNotNull<ContentModelDataList>(json['list']);
	if (list != null) {
		contentModelData.list = list;
	}
	final String? nextTime = jsonConvert.convert<String>(json['next_time']);
	if (nextTime != null) {
		contentModelData.nextTime = nextTime;
	}
	final String? currentTime = jsonConvert.convert<String>(json['current_time']);
	if (currentTime != null) {
		contentModelData.currentTime = currentTime;
	}
	final String? text = jsonConvert.convert<String>(json['text']);
	if (text != null) {
		contentModelData.text = text;
	}
	final List<dynamic>? last = jsonConvert.convertListNotNull<dynamic>(json['last']);
	if (last != null) {
		contentModelData.last = last;
	}
	final String? endText = jsonConvert.convert<String>(json['end_text']);
	if (endText != null) {
		contentModelData.endText = endText;
	}
	final String? horizontalCover = jsonConvert.convert<String>(json['horizontal_cover']);
	if (horizontalCover != null) {
		contentModelData.horizontalCover = horizontalCover;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		contentModelData.description = description;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		contentModelData.title = title;
	}
	return contentModelData;
}

Map<String, dynamic> $ContentModelDataToJson(ContentModelData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['list'] =  entity.list.map((v) => v.toJson()).toList();
	data['next_time'] = entity.nextTime;
	data['current_time'] = entity.currentTime;
	data['text'] = entity.text;
	data['last'] =  entity.last;
	data['end_text'] = entity.endText;
	data['horizontal_cover'] = entity.horizontalCover;
	data['description'] = entity.description;
	data['title'] = entity.title;
	return data;
}

ContentModelDataList $ContentModelDataListFromJson(Map<String, dynamic> json) {
	final ContentModelDataList contentModelDataList = ContentModelDataList();
	final int? comicId = jsonConvert.convert<int>(json['comic_id']);
	if (comicId != null) {
		contentModelDataList.comicId = comicId;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		contentModelDataList.title = title;
	}
	final List<String>? author = jsonConvert.convertListNotNull<String>(json['author']);
	if (author != null) {
		contentModelDataList.author = author;
	}
	final String? verticalCover = jsonConvert.convert<String>(json['vertical_cover']);
	if (verticalCover != null) {
		contentModelDataList.verticalCover = verticalCover;
	}
	final int? isFinish = jsonConvert.convert<int>(json['is_finish']);
	if (isFinish != null) {
		contentModelDataList.isFinish = isFinish;
	}
	final int? lastOrd = jsonConvert.convert<int>(json['last_ord']);
	if (lastOrd != null) {
		contentModelDataList.lastOrd = lastOrd;
	}
	final String? lastShortTitle = jsonConvert.convert<String>(json['last_short_title']);
	if (lastShortTitle != null) {
		contentModelDataList.lastShortTitle = lastShortTitle;
	}
	final List<ContentModelDataListStyles>? styles = jsonConvert.convertListNotNull<ContentModelDataListStyles>(json['styles']);
	if (styles != null) {
		contentModelDataList.styles = styles;
	}
	final int? total = jsonConvert.convert<int>(json['total']);
	if (total != null) {
		contentModelDataList.total = total;
	}
	final String? fans = jsonConvert.convert<String>(json['fans']);
	if (fans != null) {
		contentModelDataList.fans = fans;
	}
	final List<dynamic>? rewardUsers = jsonConvert.convertListNotNull<dynamic>(json['reward_users']);
	if (rewardUsers != null) {
		contentModelDataList.rewardUsers = rewardUsers;
	}
	final int? lastRank = jsonConvert.convert<int>(json['last_rank']);
	if (lastRank != null) {
		contentModelDataList.lastRank = lastRank;
	}
	final int? lastEp = jsonConvert.convert<int>(json['last_ep']);
	if (lastEp != null) {
		contentModelDataList.lastEp = lastEp;
	}
	final String? text = jsonConvert.convert<String>(json['text']);
	if (text != null) {
		contentModelDataList.text = text;
	}
	final int? rookieType = jsonConvert.convert<int>(json['rookie_type']);
	if (rookieType != null) {
		contentModelDataList.rookieType = rookieType;
	}
	final bool? allowWaitFree = jsonConvert.convert<bool>(json['allow_wait_free']);
	if (allowWaitFree != null) {
		contentModelDataList.allowWaitFree = allowWaitFree;
	}
	final int? discountType = jsonConvert.convert<int>(json['discount_type']);
	if (discountType != null) {
		contentModelDataList.discountType = discountType;
	}
	final String? homeBlockJumpValue = jsonConvert.convert<String>(json['home_block_jump_value']);
	if (homeBlockJumpValue != null) {
		contentModelDataList.homeBlockJumpValue = homeBlockJumpValue;
	}
	return contentModelDataList;
}

Map<String, dynamic> $ContentModelDataListToJson(ContentModelDataList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['comic_id'] = entity.comicId;
	data['title'] = entity.title;
	data['author'] =  entity.author;
	data['vertical_cover'] = entity.verticalCover;
	data['is_finish'] = entity.isFinish;
	data['last_ord'] = entity.lastOrd;
	data['last_short_title'] = entity.lastShortTitle;
	data['styles'] =  entity.styles.map((v) => v.toJson()).toList();
	data['total'] = entity.total;
	data['fans'] = entity.fans;
	data['reward_users'] =  entity.rewardUsers;
	data['last_rank'] = entity.lastRank;
	data['last_ep'] = entity.lastEp;
	data['text'] = entity.text;
	data['rookie_type'] = entity.rookieType;
	data['allow_wait_free'] = entity.allowWaitFree;
	data['discount_type'] = entity.discountType;
	data['home_block_jump_value'] = entity.homeBlockJumpValue;
	return data;
}

ContentModelDataListStyles $ContentModelDataListStylesFromJson(Map<String, dynamic> json) {
	final ContentModelDataListStyles contentModelDataListStyles = ContentModelDataListStyles();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		contentModelDataListStyles.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		contentModelDataListStyles.name = name;
	}
	return contentModelDataListStyles;
}

Map<String, dynamic> $ContentModelDataListStylesToJson(ContentModelDataListStyles entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}
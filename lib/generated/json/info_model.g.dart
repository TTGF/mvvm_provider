import 'package:mvvm_provider/generated/json/base/json_convert_content.dart';
import 'package:mvvm_provider/page/mine/model/info_model.dart';

InfoListModel $InfoListModelFromJson(Map<String, dynamic> json) {
	final InfoListModel infoListModel = InfoListModel();
	final List<InfoModel>? works = jsonConvert.convertListNotNull<InfoModel>(json['works']);
	if (works != null) {
		infoListModel.works = works;
	}
	final String? newsContent = jsonConvert.convert<String>(json['newsContent']);
	if (newsContent != null) {
		infoListModel.newsContent = newsContent;
	}
	final String? cover = jsonConvert.convert<String>(json['cover']);
	if (cover != null) {
		infoListModel.cover = cover;
	}
	final int? shareCount = jsonConvert.convert<int>(json['shareCount']);
	if (shareCount != null) {
		infoListModel.shareCount = shareCount;
	}
	final String? mcnIcon = jsonConvert.convert<String>(json['mcnIcon']);
	if (mcnIcon != null) {
		infoListModel.mcnIcon = mcnIcon;
	}
	final String? editorName = jsonConvert.convert<String>(json['editorName']);
	if (editorName != null) {
		infoListModel.editorName = editorName;
	}
	final int? mcnTemplateId = jsonConvert.convert<int>(json['mcnTemplateId']);
	if (mcnTemplateId != null) {
		infoListModel.mcnTemplateId = mcnTemplateId;
	}
	final int? followStatus = jsonConvert.convert<int>(json['followStatus']);
	if (followStatus != null) {
		infoListModel.followStatus = followStatus;
	}
	final int? entityType = jsonConvert.convert<int>(json['entityType']);
	if (entityType != null) {
		infoListModel.entityType = entityType;
	}
	final String? nickName = jsonConvert.convert<String>(json['nickName']);
	if (nickName != null) {
		infoListModel.nickName = nickName;
	}
	final bool? isThumbs = jsonConvert.convert<bool>(json['isThumbs']);
	if (isThumbs != null) {
		infoListModel.isThumbs = isThumbs;
	}
	final int? commentCount = jsonConvert.convert<int>(json['commentCount']);
	if (commentCount != null) {
		infoListModel.commentCount = commentCount;
	}
	final String? pageViewStr = jsonConvert.convert<String>(json['pageViewStr']);
	if (pageViewStr != null) {
		infoListModel.pageViewStr = pageViewStr;
	}
	final String? shareCountStr = jsonConvert.convert<String>(json['shareCountStr']);
	if (shareCountStr != null) {
		infoListModel.shareCountStr = shareCountStr;
	}
	final int? subType = jsonConvert.convert<int>(json['subType']);
	if (subType != null) {
		infoListModel.subType = subType;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		infoListModel.status = status;
	}
	final String? mcnRealName = jsonConvert.convert<String>(json['mcnRealName']);
	if (mcnRealName != null) {
		infoListModel.mcnRealName = mcnRealName;
	}
	final String? creationTime = jsonConvert.convert<String>(json['creationTime']);
	if (creationTime != null) {
		infoListModel.creationTime = creationTime;
	}
	final bool? isLike = jsonConvert.convert<bool>(json['isLike']);
	if (isLike != null) {
		infoListModel.isLike = isLike;
	}
	final String? userAvatar = jsonConvert.convert<String>(json['userAvatar']);
	if (userAvatar != null) {
		infoListModel.userAvatar = userAvatar;
	}
	final String? mcnPageView = jsonConvert.convert<String>(json['mcnPageView']);
	if (mcnPageView != null) {
		infoListModel.mcnPageView = mcnPageView;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		infoListModel.description = description;
	}
	final String? commentCountStr = jsonConvert.convert<String>(json['commentCountStr']);
	if (commentCountStr != null) {
		infoListModel.commentCountStr = commentCountStr;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		infoListModel.title = title;
	}
	final String? mcnFansCount = jsonConvert.convert<String>(json['mcnFansCount']);
	if (mcnFansCount != null) {
		infoListModel.mcnFansCount = mcnFansCount;
	}
	final String? brandLogo = jsonConvert.convert<String>(json['brandLogo']);
	if (brandLogo != null) {
		infoListModel.brandLogo = brandLogo;
	}
	final int? channelId = jsonConvert.convert<int>(json['channelId']);
	if (channelId != null) {
		infoListModel.channelId = channelId;
	}
	final String? thumbsCounts = jsonConvert.convert<String>(json['thumbsCounts']);
	if (thumbsCounts != null) {
		infoListModel.thumbsCounts = thumbsCounts;
	}
	final int? original = jsonConvert.convert<int>(json['original']);
	if (original != null) {
		infoListModel.original = original;
	}
	final String? uVContent = jsonConvert.convert<String>(json['uVContent']);
	if (uVContent != null) {
		infoListModel.uVContent = uVContent;
	}
	final int? entityId = jsonConvert.convert<int>(json['entityId']);
	if (entityId != null) {
		infoListModel.entityId = entityId;
	}
	final String? userName = jsonConvert.convert<String>(json['userName']);
	if (userName != null) {
		infoListModel.userName = userName;
	}
	final String? userId = jsonConvert.convert<String>(json['userId']);
	if (userId != null) {
		infoListModel.userId = userId;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		infoListModel.url = url;
	}
	final int? pageView = jsonConvert.convert<int>(json['pageView']);
	if (pageView != null) {
		infoListModel.pageView = pageView;
	}
	final String? createTime = jsonConvert.convert<String>(json['createTime']);
	if (createTime != null) {
		infoListModel.createTime = createTime;
	}
	final int? mcnId = jsonConvert.convert<int>(json['mcnId']);
	if (mcnId != null) {
		infoListModel.mcnId = mcnId;
	}
	final int? categoryId = jsonConvert.convert<int>(json['categoryId']);
	if (categoryId != null) {
		infoListModel.categoryId = categoryId;
	}
	return infoListModel;
}

Map<String, dynamic> $InfoListModelToJson(InfoListModel entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['works'] =  entity.works.map((v) => v.toJson()).toList();
	data['newsContent'] = entity.newsContent;
	data['cover'] = entity.cover;
	data['shareCount'] = entity.shareCount;
	data['mcnIcon'] = entity.mcnIcon;
	data['editorName'] = entity.editorName;
	data['mcnTemplateId'] = entity.mcnTemplateId;
	data['followStatus'] = entity.followStatus;
	data['entityType'] = entity.entityType;
	data['nickName'] = entity.nickName;
	data['isThumbs'] = entity.isThumbs;
	data['commentCount'] = entity.commentCount;
	data['pageViewStr'] = entity.pageViewStr;
	data['shareCountStr'] = entity.shareCountStr;
	data['subType'] = entity.subType;
	data['status'] = entity.status;
	data['mcnRealName'] = entity.mcnRealName;
	data['creationTime'] = entity.creationTime;
	data['isLike'] = entity.isLike;
	data['userAvatar'] = entity.userAvatar;
	data['mcnPageView'] = entity.mcnPageView;
	data['description'] = entity.description;
	data['commentCountStr'] = entity.commentCountStr;
	data['title'] = entity.title;
	data['mcnFansCount'] = entity.mcnFansCount;
	data['brandLogo'] = entity.brandLogo;
	data['channelId'] = entity.channelId;
	data['thumbsCounts'] = entity.thumbsCounts;
	data['original'] = entity.original;
	data['uVContent'] = entity.uVContent;
	data['entityId'] = entity.entityId;
	data['userName'] = entity.userName;
	data['userId'] = entity.userId;
	data['url'] = entity.url;
	data['pageView'] = entity.pageView;
	data['createTime'] = entity.createTime;
	data['mcnId'] = entity.mcnId;
	data['categoryId'] = entity.categoryId;
	return data;
}

InfoModel $InfoModelFromJson(Map<String, dynamic> json) {
	final InfoModel infoModel = InfoModel();
	final String? mcnRealName = jsonConvert.convert<String>(json['mcnRealName']);
	if (mcnRealName != null) {
		infoModel.mcnRealName = mcnRealName;
	}
	final String? uVContentStr = jsonConvert.convert<String>(json['uVContentStr']);
	if (uVContentStr != null) {
		infoModel.uVContentStr = uVContentStr;
	}
	final int? adFlag = jsonConvert.convert<int>(json['adFlag']);
	if (adFlag != null) {
		infoModel.adFlag = adFlag;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		infoModel.description = description;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		infoModel.title = title;
	}
	final int? checkStatus = jsonConvert.convert<int>(json['checkStatus']);
	if (checkStatus != null) {
		infoModel.checkStatus = checkStatus;
	}
	final String? mcnIcon = jsonConvert.convert<String>(json['mcnIcon']);
	if (mcnIcon != null) {
		infoModel.mcnIcon = mcnIcon;
	}
	final int? adFlagShow = jsonConvert.convert<int>(json['adFlagShow']);
	if (adFlagShow != null) {
		infoModel.adFlagShow = adFlagShow;
	}
	final int? adsType = jsonConvert.convert<int>(json['adsType']);
	if (adsType != null) {
		infoModel.adsType = adsType;
	}
	final int? worksType = jsonConvert.convert<int>(json['worksType']);
	if (worksType != null) {
		infoModel.worksType = worksType;
	}
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		infoModel.id = id;
	}
	final int? uVContent = jsonConvert.convert<int>(json['uVContent']);
	if (uVContent != null) {
		infoModel.uVContent = uVContent;
	}
	final String? avatarUrl = jsonConvert.convert<String>(json['avatarUrl']);
	if (avatarUrl != null) {
		infoModel.avatarUrl = avatarUrl;
	}
	final int? entityType = jsonConvert.convert<int>(json['entityType']);
	if (entityType != null) {
		infoModel.entityType = entityType;
	}
	final String? nickName = jsonConvert.convert<String>(json['nickName']);
	if (nickName != null) {
		infoModel.nickName = nickName;
	}
	final int? userId = jsonConvert.convert<int>(json['userId']);
	if (userId != null) {
		infoModel.userId = userId;
	}
	final String? url = jsonConvert.convert<String>(json['url']);
	if (url != null) {
		infoModel.url = url;
	}
	final String? createTime = jsonConvert.convert<String>(json['createTime']);
	if (createTime != null) {
		infoModel.createTime = createTime;
	}
	final String? createTimeStr = jsonConvert.convert<String>(json['createTimeStr']);
	if (createTimeStr != null) {
		infoModel.createTimeStr = createTimeStr;
	}
	final String? videoCover = jsonConvert.convert<String>(json['videoCover']);
	if (videoCover != null) {
		infoModel.videoCover = videoCover;
	}
	final List<String>? newsPics = jsonConvert.convertListNotNull<String>(json['newsPics']);
	if (newsPics != null) {
		infoModel.newsPics = newsPics;
	}
	final int? isThumbs = jsonConvert.convert<int>(json['isThumbs']);
	if (isThumbs != null) {
		infoModel.isThumbs = isThumbs;
	}
	return infoModel;
}

Map<String, dynamic> $InfoModelToJson(InfoModel entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['mcnRealName'] = entity.mcnRealName;
	data['uVContentStr'] = entity.uVContentStr;
	data['adFlag'] = entity.adFlag;
	data['description'] = entity.description;
	data['title'] = entity.title;
	data['checkStatus'] = entity.checkStatus;
	data['mcnIcon'] = entity.mcnIcon;
	data['adFlagShow'] = entity.adFlagShow;
	data['adsType'] = entity.adsType;
	data['worksType'] = entity.worksType;
	data['id'] = entity.id;
	data['uVContent'] = entity.uVContent;
	data['avatarUrl'] = entity.avatarUrl;
	data['entityType'] = entity.entityType;
	data['nickName'] = entity.nickName;
	data['userId'] = entity.userId;
	data['url'] = entity.url;
	data['createTime'] = entity.createTime;
	data['createTimeStr'] = entity.createTimeStr;
	data['videoCover'] = entity.videoCover;
	data['newsPics'] =  entity.newsPics;
	data['isThumbs'] = entity.isThumbs;
	return data;
}
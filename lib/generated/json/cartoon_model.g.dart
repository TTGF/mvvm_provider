import 'package:mvvm_provider/generated/json/base/json_convert_content.dart';
import 'package:mvvm_provider/page/home/model/cartoon_model.dart';

CarDataModel $CarDataModelFromJson(Map<String, dynamic> json) {
	final CarDataModel carDataModel = CarDataModel();
	final List<CartoonModel>? feeds = jsonConvert.convertListNotNull<CartoonModel>(json['feeds']);
	if (feeds != null) {
		carDataModel.feeds = feeds;
	}
	final int? show_times = jsonConvert.convert<int>(json['show_times']);
	if (show_times != null) {
		carDataModel.show_times = show_times;
	}
	return carDataModel;
}

Map<String, dynamic> $CarDataModelToJson(CarDataModel entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['feeds'] =  entity.feeds?.map((v) => v.toJson()).toList();
	data['show_times'] = entity.show_times;
	return data;
}

CartoonModel $CartoonModelFromJson(Map<String, dynamic> json) {
	final CartoonModel cartoonModel = CartoonModel();
	final String? item_id = jsonConvert.convert<String>(json['item_id']);
	if (item_id != null) {
		cartoonModel.item_id = item_id;
	}
	final String? parent_id = jsonConvert.convert<String>(json['parent_id']);
	if (parent_id != null) {
		cartoonModel.parent_id = parent_id;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		cartoonModel.title = title;
	}
	final String? jump_value = jsonConvert.convert<String>(json['jump_value']);
	if (jump_value != null) {
		cartoonModel.jump_value = jump_value;
	}
	final int? type = jsonConvert.convert<int>(json['type']);
	if (type != null) {
		cartoonModel.type = type;
	}
	final String? image = jsonConvert.convert<String>(json['image']);
	if (image != null) {
		cartoonModel.image = image;
	}
	final int? crossAxisCount = jsonConvert.convert<int>(json['crossAxisCount']);
	if (crossAxisCount != null) {
		cartoonModel.crossAxisCount = crossAxisCount;
	}
	final int? mainAxisCount = jsonConvert.convert<int>(json['mainAxisCount']);
	if (mainAxisCount != null) {
		cartoonModel.mainAxisCount = mainAxisCount;
	}
	final CommicInfoModel? comic_info = jsonConvert.convert<CommicInfoModel>(json['comic_info']);
	if (comic_info != null) {
		cartoonModel.comic_info = comic_info;
	}
	return cartoonModel;
}

Map<String, dynamic> $CartoonModelToJson(CartoonModel entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['item_id'] = entity.item_id;
	data['parent_id'] = entity.parent_id;
	data['title'] = entity.title;
	data['jump_value'] = entity.jump_value;
	data['type'] = entity.type;
	data['image'] = entity.image;
	data['crossAxisCount'] = entity.crossAxisCount;
	data['mainAxisCount'] = entity.mainAxisCount;
	data['comic_info'] = entity.comic_info?.toJson();
	return data;
}

CommicInfoModel $CommicInfoModelFromJson(Map<String, dynamic> json) {
	final CommicInfoModel commicInfoModel = CommicInfoModel();
	final String? decision = jsonConvert.convert<String>(json['decision']);
	if (decision != null) {
		commicInfoModel.decision = decision;
	}
	final String? lastest_short_title = jsonConvert.convert<String>(json['lastest_short_title']);
	if (lastest_short_title != null) {
		commicInfoModel.lastest_short_title = lastest_short_title;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		commicInfoModel.title = title;
	}
	final int? is_finish = jsonConvert.convert<int>(json['is_finish']);
	if (is_finish != null) {
		commicInfoModel.is_finish = is_finish;
	}
	final String? main_style_name = jsonConvert.convert<String>(json['main_style_name']);
	if (main_style_name != null) {
		commicInfoModel.main_style_name = main_style_name;
	}
	final double? score = jsonConvert.convert<double>(json['score']);
	if (score != null) {
		commicInfoModel.score = score;
	}
	final List<String>? styles = jsonConvert.convertListNotNull<String>(json['styles']);
	if (styles != null) {
		commicInfoModel.styles = styles;
	}
	final List<String>? tags = jsonConvert.convertListNotNull<String>(json['tags']);
	if (tags != null) {
		commicInfoModel.tags = tags;
	}
	return commicInfoModel;
}

Map<String, dynamic> $CommicInfoModelToJson(CommicInfoModel entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['decision'] = entity.decision;
	data['lastest_short_title'] = entity.lastest_short_title;
	data['title'] = entity.title;
	data['is_finish'] = entity.is_finish;
	data['main_style_name'] = entity.main_style_name;
	data['score'] = entity.score;
	data['styles'] =  entity.styles;
	data['tags'] =  entity.tags;
	return data;
}
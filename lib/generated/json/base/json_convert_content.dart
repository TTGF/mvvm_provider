// ignore_for_file: non_constant_identifier_names
// ignore_for_file: camel_case_types
// ignore_for_file: prefer_single_quotes

// This file is automatically generated. DO NOT EDIT, all your changes would be lost.
import 'package:flutter/material.dart' show debugPrint;
import 'package:mvvm_provider/model/banner_model.dart';
import 'package:mvvm_provider/page/home/model/cartoon_model.dart';
import 'package:mvvm_provider/page/home/model/cartoon_model_entity.dart';
import 'package:mvvm_provider/page/home/model/cartoon_recommend_entity.dart';
import 'package:mvvm_provider/page/home/model/cartoon_series_entity.dart';
import 'package:mvvm_provider/page/mine/model/info_model.dart';
import 'package:mvvm_provider/page/novel/model/content_model_entity.dart';
import 'package:mvvm_provider/page/novel/model/tabs_model_entity.dart';

JsonConvert jsonConvert = JsonConvert();
typedef JsonConvertFunction<T> = T Function(Map<String, dynamic> json);
typedef EnumConvertFunction<T> = T Function(String value);

class JsonConvert {
	static final Map<String, JsonConvertFunction> convertFuncMap = {
		(BannerModel).toString(): BannerModel.fromJson,
		(CarDataModel).toString(): CarDataModel.fromJson,
		(CartoonModel).toString(): CartoonModel.fromJson,
		(CommicInfoModel).toString(): CommicInfoModel.fromJson,
		(CartoonModelData).toString(): CartoonModelData.fromJson,
		(CartoonModelDataAuthor).toString(): CartoonModelDataAuthor.fromJson,
		(CartoonModelDataCover).toString(): CartoonModelDataCover.fromJson,
		(CartoonModelDataStyle).toString(): CartoonModelDataStyle.fromJson,
		(CartoonModelDataTag).toString(): CartoonModelDataTag.fromJson,
		(CartoonModelDataEp).toString(): CartoonModelDataEp.fromJson,
		(CartoonModelDataCopyright).toString(): CartoonModelDataCopyright.fromJson,
		(CartoonModelDataStyles).toString(): CartoonModelDataStyles.fromJson,
		(CartoonRecommendData).toString(): CartoonRecommendData.fromJson,
		(CartoonRecommendDataInfos).toString(): CartoonRecommendDataInfos.fromJson,
		(CartoonRecommendDataInfosAuthor).toString(): CartoonRecommendDataInfosAuthor.fromJson,
		(CartoonRecommendDataInfosCover).toString(): CartoonRecommendDataInfosCover.fromJson,
		(CartoonRecommendDataInfosStyle).toString(): CartoonRecommendDataInfosStyle.fromJson,
		(CartoonRecommendDataInfosTag).toString(): CartoonRecommendDataInfosTag.fromJson,
		(CartoonSeriesData).toString(): CartoonSeriesData.fromJson,
		(CartoonSeriesDataSeriesComics).toString(): CartoonSeriesDataSeriesComics.fromJson,
		(CartoonSeriesDataSeriesComicsStyles).toString(): CartoonSeriesDataSeriesComicsStyles.fromJson,
		(InfoListModel).toString(): InfoListModel.fromJson,
		(InfoModel).toString(): InfoModel.fromJson,
		(ContentModelData).toString(): ContentModelData.fromJson,
		(ContentModelDataList).toString(): ContentModelDataList.fromJson,
		(ContentModelDataListStyles).toString(): ContentModelDataListStyles.fromJson,
		(TabsModelData).toString(): TabsModelData.fromJson,
		(TabsModelDataList).toString(): TabsModelDataList.fromJson,
	};

  T? convert<T>(dynamic value, {EnumConvertFunction? enumConvert}) {
    if (value == null) {
      return null;
    }
    if (value is T) {
      return value;
    }
    try {
      return _asT<T>(value, enumConvert: enumConvert);
    } catch (e, stackTrace) {
      debugPrint('asT<$T> $e $stackTrace');
      return null;
    }
  }

  List<T?>? convertList<T>(List<dynamic>? value, {EnumConvertFunction? enumConvert}) {
    if (value == null) {
      return null;
    }
    try {
      return value.map((dynamic e) => _asT<T>(e,enumConvert: enumConvert)).toList();
    } catch (e, stackTrace) {
      debugPrint('asT<$T> $e $stackTrace');
      return <T>[];
    }
  }

List<T>? convertListNotNull<T>(dynamic value, {EnumConvertFunction? enumConvert}) {
    if (value == null) {
      return null;
    }
    try {
      return (value as List<dynamic>).map((dynamic e) => _asT<T>(e,enumConvert: enumConvert)!).toList();
    } catch (e, stackTrace) {
      debugPrint('asT<$T> $e $stackTrace');
      return <T>[];
    }
  }

  T? _asT<T extends Object?>(dynamic value,
      {EnumConvertFunction? enumConvert}) {
    final String type = T.toString();
    final String valueS = value.toString();
    if (enumConvert != null) {
      return enumConvert(valueS) as T;
    } else if (type == "String") {
      return valueS as T;
    } else if (type == "int") {
      final int? intValue = int.tryParse(valueS);
      if (intValue == null) {
        return double.tryParse(valueS)?.toInt() as T?;
      } else {
        return intValue as T;
      }
    } else if (type == "double") {
      return double.parse(valueS) as T;
    } else if (type == "DateTime") {
      return DateTime.parse(valueS) as T;
    } else if (type == "bool") {
      if (valueS == '0' || valueS == '1') {
        return (valueS == '1') as T;
      }
      return (valueS == 'true') as T;
    } else if (type == "Map" || type.startsWith("Map<")) {
      return value as T;
    } else {
      if (convertFuncMap.containsKey(type)) {
        return convertFuncMap[type]!(Map<String, dynamic>.from(value)) as T;
      } else {
        throw UnimplementedError('$type unimplemented');
      }
    }
  }

	//list is returned by type
	static M? _getListChildType<M>(List<Map<String, dynamic>> data) {
		if(<BannerModel>[] is M){
			return data.map<BannerModel>((Map<String, dynamic> e) => BannerModel.fromJson(e)).toList() as M;
		}
		if(<CarDataModel>[] is M){
			return data.map<CarDataModel>((Map<String, dynamic> e) => CarDataModel.fromJson(e)).toList() as M;
		}
		if(<CartoonModel>[] is M){
			return data.map<CartoonModel>((Map<String, dynamic> e) => CartoonModel.fromJson(e)).toList() as M;
		}
		if(<CommicInfoModel>[] is M){
			return data.map<CommicInfoModel>((Map<String, dynamic> e) => CommicInfoModel.fromJson(e)).toList() as M;
		}
		if(<CartoonModelData>[] is M){
			return data.map<CartoonModelData>((Map<String, dynamic> e) => CartoonModelData.fromJson(e)).toList() as M;
		}
		if(<CartoonModelDataAuthor>[] is M){
			return data.map<CartoonModelDataAuthor>((Map<String, dynamic> e) => CartoonModelDataAuthor.fromJson(e)).toList() as M;
		}
		if(<CartoonModelDataCover>[] is M){
			return data.map<CartoonModelDataCover>((Map<String, dynamic> e) => CartoonModelDataCover.fromJson(e)).toList() as M;
		}
		if(<CartoonModelDataStyle>[] is M){
			return data.map<CartoonModelDataStyle>((Map<String, dynamic> e) => CartoonModelDataStyle.fromJson(e)).toList() as M;
		}
		if(<CartoonModelDataTag>[] is M){
			return data.map<CartoonModelDataTag>((Map<String, dynamic> e) => CartoonModelDataTag.fromJson(e)).toList() as M;
		}
		if(<CartoonModelDataEp>[] is M){
			return data.map<CartoonModelDataEp>((Map<String, dynamic> e) => CartoonModelDataEp.fromJson(e)).toList() as M;
		}
		if(<CartoonModelDataCopyright>[] is M){
			return data.map<CartoonModelDataCopyright>((Map<String, dynamic> e) => CartoonModelDataCopyright.fromJson(e)).toList() as M;
		}
		if(<CartoonModelDataStyles>[] is M){
			return data.map<CartoonModelDataStyles>((Map<String, dynamic> e) => CartoonModelDataStyles.fromJson(e)).toList() as M;
		}
		if(<CartoonRecommendData>[] is M){
			return data.map<CartoonRecommendData>((Map<String, dynamic> e) => CartoonRecommendData.fromJson(e)).toList() as M;
		}
		if(<CartoonRecommendDataInfos>[] is M){
			return data.map<CartoonRecommendDataInfos>((Map<String, dynamic> e) => CartoonRecommendDataInfos.fromJson(e)).toList() as M;
		}
		if(<CartoonRecommendDataInfosAuthor>[] is M){
			return data.map<CartoonRecommendDataInfosAuthor>((Map<String, dynamic> e) => CartoonRecommendDataInfosAuthor.fromJson(e)).toList() as M;
		}
		if(<CartoonRecommendDataInfosCover>[] is M){
			return data.map<CartoonRecommendDataInfosCover>((Map<String, dynamic> e) => CartoonRecommendDataInfosCover.fromJson(e)).toList() as M;
		}
		if(<CartoonRecommendDataInfosStyle>[] is M){
			return data.map<CartoonRecommendDataInfosStyle>((Map<String, dynamic> e) => CartoonRecommendDataInfosStyle.fromJson(e)).toList() as M;
		}
		if(<CartoonRecommendDataInfosTag>[] is M){
			return data.map<CartoonRecommendDataInfosTag>((Map<String, dynamic> e) => CartoonRecommendDataInfosTag.fromJson(e)).toList() as M;
		}
		if(<CartoonSeriesData>[] is M){
			return data.map<CartoonSeriesData>((Map<String, dynamic> e) => CartoonSeriesData.fromJson(e)).toList() as M;
		}
		if(<CartoonSeriesDataSeriesComics>[] is M){
			return data.map<CartoonSeriesDataSeriesComics>((Map<String, dynamic> e) => CartoonSeriesDataSeriesComics.fromJson(e)).toList() as M;
		}
		if(<CartoonSeriesDataSeriesComicsStyles>[] is M){
			return data.map<CartoonSeriesDataSeriesComicsStyles>((Map<String, dynamic> e) => CartoonSeriesDataSeriesComicsStyles.fromJson(e)).toList() as M;
		}
		if(<InfoListModel>[] is M){
			return data.map<InfoListModel>((Map<String, dynamic> e) => InfoListModel.fromJson(e)).toList() as M;
		}
		if(<InfoModel>[] is M){
			return data.map<InfoModel>((Map<String, dynamic> e) => InfoModel.fromJson(e)).toList() as M;
		}
		if(<ContentModelData>[] is M){
			return data.map<ContentModelData>((Map<String, dynamic> e) => ContentModelData.fromJson(e)).toList() as M;
		}
		if(<ContentModelDataList>[] is M){
			return data.map<ContentModelDataList>((Map<String, dynamic> e) => ContentModelDataList.fromJson(e)).toList() as M;
		}
		if(<ContentModelDataListStyles>[] is M){
			return data.map<ContentModelDataListStyles>((Map<String, dynamic> e) => ContentModelDataListStyles.fromJson(e)).toList() as M;
		}
		if(<TabsModelData>[] is M){
			return data.map<TabsModelData>((Map<String, dynamic> e) => TabsModelData.fromJson(e)).toList() as M;
		}
		if(<TabsModelDataList>[] is M){
			return data.map<TabsModelDataList>((Map<String, dynamic> e) => TabsModelDataList.fromJson(e)).toList() as M;
		}

		debugPrint("${M.toString()} not found");
	
		return null;
}

	static M? fromJsonAsT<M>(dynamic json) {
		if (json is List) {
			return _getListChildType<M>(json.map((e) => e as Map<String, dynamic>).toList());
		} else {
			return jsonConvert.convert<M>(json);
		}
	}
}
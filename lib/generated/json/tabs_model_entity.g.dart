import 'package:mvvm_provider/generated/json/base/json_convert_content.dart';
import 'package:mvvm_provider/page/novel/model/tabs_model_entity.dart';

TabsModelData $TabsModelDataFromJson(Map<String, dynamic> json) {
	final TabsModelData tabsModelData = TabsModelData();
	final List<TabsModelDataList>? list = jsonConvert.convertListNotNull<TabsModelDataList>(json['list']);
	if (list != null) {
		tabsModelData.list = list;
	}
	final int? defaultId = jsonConvert.convert<int>(json['default_id']);
	if (defaultId != null) {
		tabsModelData.defaultId = defaultId;
	}
	return tabsModelData;
}

Map<String, dynamic> $TabsModelDataToJson(TabsModelData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['list'] =  entity.list.map((v) => v.toJson()).toList();
	data['default_id'] = entity.defaultId;
	return data;
}

TabsModelDataList $TabsModelDataListFromJson(Map<String, dynamic> json) {
	final TabsModelDataList tabsModelDataList = TabsModelDataList();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		tabsModelDataList.id = id;
	}
	final int? type = jsonConvert.convert<int>(json['type']);
	if (type != null) {
		tabsModelDataList.type = type;
	}
	final String? description = jsonConvert.convert<String>(json['description']);
	if (description != null) {
		tabsModelDataList.description = description;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		tabsModelDataList.name = name;
	}
	return tabsModelDataList;
}

Map<String, dynamic> $TabsModelDataListToJson(TabsModelDataList entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['type'] = entity.type;
	data['description'] = entity.description;
	data['name'] = entity.name;
	return data;
}
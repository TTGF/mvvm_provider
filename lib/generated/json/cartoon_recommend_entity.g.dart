import 'package:mvvm_provider/generated/json/base/json_convert_content.dart';
import 'package:mvvm_provider/page/home/model/cartoon_recommend_entity.dart';

CartoonRecommendData $CartoonRecommendDataFromJson(Map<String, dynamic> json) {
	final CartoonRecommendData cartoonRecommendData = CartoonRecommendData();
	final List<CartoonRecommendDataInfos>? infos = jsonConvert.convertListNotNull<CartoonRecommendDataInfos>(json['infos']);
	if (infos != null) {
		cartoonRecommendData.infos = infos;
	}
	return cartoonRecommendData;
}

Map<String, dynamic> $CartoonRecommendDataToJson(CartoonRecommendData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['infos'] =  entity.infos.map((v) => v.toJson()).toList();
	return data;
}

CartoonRecommendDataInfos $CartoonRecommendDataInfosFromJson(Map<String, dynamic> json) {
	final CartoonRecommendDataInfos cartoonRecommendDataInfos = CartoonRecommendDataInfos();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		cartoonRecommendDataInfos.id = id;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		cartoonRecommendDataInfos.title = title;
	}
	final CartoonRecommendDataInfosAuthor? author = jsonConvert.convert<CartoonRecommendDataInfosAuthor>(json['author']);
	if (author != null) {
		cartoonRecommendDataInfos.author = author;
	}
	final CartoonRecommendDataInfosCover? cover = jsonConvert.convert<CartoonRecommendDataInfosCover>(json['cover']);
	if (cover != null) {
		cartoonRecommendDataInfos.cover = cover;
	}
	final int? sameAuthor = jsonConvert.convert<int>(json['same_author']);
	if (sameAuthor != null) {
		cartoonRecommendDataInfos.sameAuthor = sameAuthor;
	}
	final int? finishStatus = jsonConvert.convert<int>(json['finish_status']);
	if (finishStatus != null) {
		cartoonRecommendDataInfos.finishStatus = finishStatus;
	}
	final int? epTotal = jsonConvert.convert<int>(json['ep_total']);
	if (epTotal != null) {
		cartoonRecommendDataInfos.epTotal = epTotal;
	}
	final CartoonRecommendDataInfosStyle? style = jsonConvert.convert<CartoonRecommendDataInfosStyle>(json['style']);
	if (style != null) {
		cartoonRecommendDataInfos.style = style;
	}
	final List<CartoonRecommendDataInfosTag>? tag = jsonConvert.convertListNotNull<CartoonRecommendDataInfosTag>(json['tag']);
	if (tag != null) {
		cartoonRecommendDataInfos.tag = tag;
	}
	return cartoonRecommendDataInfos;
}

Map<String, dynamic> $CartoonRecommendDataInfosToJson(CartoonRecommendDataInfos entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['title'] = entity.title;
	data['author'] = entity.author.toJson();
	data['cover'] = entity.cover.toJson();
	data['same_author'] = entity.sameAuthor;
	data['finish_status'] = entity.finishStatus;
	data['ep_total'] = entity.epTotal;
	data['style'] = entity.style.toJson();
	data['tag'] =  entity.tag.map((v) => v.toJson()).toList();
	return data;
}

CartoonRecommendDataInfosAuthor $CartoonRecommendDataInfosAuthorFromJson(Map<String, dynamic> json) {
	final CartoonRecommendDataInfosAuthor cartoonRecommendDataInfosAuthor = CartoonRecommendDataInfosAuthor();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		cartoonRecommendDataInfosAuthor.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		cartoonRecommendDataInfosAuthor.name = name;
	}
	return cartoonRecommendDataInfosAuthor;
}

Map<String, dynamic> $CartoonRecommendDataInfosAuthorToJson(CartoonRecommendDataInfosAuthor entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

CartoonRecommendDataInfosCover $CartoonRecommendDataInfosCoverFromJson(Map<String, dynamic> json) {
	final CartoonRecommendDataInfosCover cartoonRecommendDataInfosCover = CartoonRecommendDataInfosCover();
	final String? vCover = jsonConvert.convert<String>(json['v_cover']);
	if (vCover != null) {
		cartoonRecommendDataInfosCover.vCover = vCover;
	}
	final String? sCover = jsonConvert.convert<String>(json['s_cover']);
	if (sCover != null) {
		cartoonRecommendDataInfosCover.sCover = sCover;
	}
	return cartoonRecommendDataInfosCover;
}

Map<String, dynamic> $CartoonRecommendDataInfosCoverToJson(CartoonRecommendDataInfosCover entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['v_cover'] = entity.vCover;
	data['s_cover'] = entity.sCover;
	return data;
}

CartoonRecommendDataInfosStyle $CartoonRecommendDataInfosStyleFromJson(Map<String, dynamic> json) {
	final CartoonRecommendDataInfosStyle cartoonRecommendDataInfosStyle = CartoonRecommendDataInfosStyle();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		cartoonRecommendDataInfosStyle.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		cartoonRecommendDataInfosStyle.name = name;
	}
	final int? pId = jsonConvert.convert<int>(json['p_id']);
	if (pId != null) {
		cartoonRecommendDataInfosStyle.pId = pId;
	}
	return cartoonRecommendDataInfosStyle;
}

Map<String, dynamic> $CartoonRecommendDataInfosStyleToJson(CartoonRecommendDataInfosStyle entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['p_id'] = entity.pId;
	return data;
}

CartoonRecommendDataInfosTag $CartoonRecommendDataInfosTagFromJson(Map<String, dynamic> json) {
	final CartoonRecommendDataInfosTag cartoonRecommendDataInfosTag = CartoonRecommendDataInfosTag();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		cartoonRecommendDataInfosTag.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		cartoonRecommendDataInfosTag.name = name;
	}
	return cartoonRecommendDataInfosTag;
}

Map<String, dynamic> $CartoonRecommendDataInfosTagToJson(CartoonRecommendDataInfosTag entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}
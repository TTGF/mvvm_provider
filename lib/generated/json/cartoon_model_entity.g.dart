import 'package:mvvm_provider/generated/json/base/json_convert_content.dart';
import 'package:mvvm_provider/page/home/model/cartoon_model_entity.dart';

CartoonModelData $CartoonModelDataFromJson(Map<String, dynamic> json) {
	final CartoonModelData cartoonModelData = CartoonModelData();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		cartoonModelData.id = id;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		cartoonModelData.title = title;
	}
	final CartoonModelDataAuthor? author = jsonConvert.convert<CartoonModelDataAuthor>(json['author']);
	if (author != null) {
		cartoonModelData.author = author;
	}
	final CartoonModelDataCover? cover = jsonConvert.convert<CartoonModelDataCover>(json['cover']);
	if (cover != null) {
		cartoonModelData.cover = cover;
	}
	final int? finishStatus = jsonConvert.convert<int>(json['finish_status']);
	if (finishStatus != null) {
		cartoonModelData.finishStatus = finishStatus;
	}
	final int? number = jsonConvert.convert<int>(json['number']);
	if (number != null) {
		cartoonModelData.number = number;
	}
	final int? epTotal = jsonConvert.convert<int>(json['ep_total']);
	if (epTotal != null) {
		cartoonModelData.epTotal = epTotal;
	}
	final CartoonModelDataStyle? style = jsonConvert.convert<CartoonModelDataStyle>(json['style']);
	if (style != null) {
		cartoonModelData.style = style;
	}
	final List<CartoonModelDataTag>? tag = jsonConvert.convertListNotNull<CartoonModelDataTag>(json['tag']);
	if (tag != null) {
		cartoonModelData.tag = tag;
	}
	final String? evaluate = jsonConvert.convert<String>(json['evaluate']);
	if (evaluate != null) {
		cartoonModelData.evaluate = evaluate;
	}
	final List<CartoonModelDataEp>? ep = jsonConvert.convertListNotNull<CartoonModelDataEp>(json['ep']);
	if (ep != null) {
		cartoonModelData.ep = ep;
	}
	final int? autoPayStatus = jsonConvert.convert<int>(json['auto_pay_status']);
	if (autoPayStatus != null) {
		cartoonModelData.autoPayStatus = autoPayStatus;
	}
	final CartoonModelDataCopyright? copyright = jsonConvert.convert<CartoonModelDataCopyright>(json['copyright']);
	if (copyright != null) {
		cartoonModelData.copyright = copyright;
	}
	final int? favorite = jsonConvert.convert<int>(json['favorite']);
	if (favorite != null) {
		cartoonModelData.favorite = favorite;
	}
	final List<dynamic>? discount = jsonConvert.convertListNotNull<dynamic>(json['discount']);
	if (discount != null) {
		cartoonModelData.discount = discount;
	}
	final int? status = jsonConvert.convert<int>(json['status']);
	if (status != null) {
		cartoonModelData.status = status;
	}
	final String? commentId = jsonConvert.convert<String>(json['comment_id']);
	if (commentId != null) {
		cartoonModelData.commentId = commentId;
	}
	final int? commentState = jsonConvert.convert<int>(json['comment_state']);
	if (commentState != null) {
		cartoonModelData.commentState = commentState;
	}
	final int? commentShareState = jsonConvert.convert<int>(json['comment_share_state']);
	if (commentShareState != null) {
		cartoonModelData.commentShareState = commentShareState;
	}
	final int? discountState = jsonConvert.convert<int>(json['discount_state']);
	if (discountState != null) {
		cartoonModelData.discountState = discountState;
	}
	final List<CartoonModelDataStyles>? styles = jsonConvert.convertListNotNull<CartoonModelDataStyles>(json['styles']);
	if (styles != null) {
		cartoonModelData.styles = styles;
	}
	return cartoonModelData;
}

Map<String, dynamic> $CartoonModelDataToJson(CartoonModelData entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['title'] = entity.title;
	data['author'] = entity.author.toJson();
	data['cover'] = entity.cover.toJson();
	data['finish_status'] = entity.finishStatus;
	data['number'] = entity.number;
	data['ep_total'] = entity.epTotal;
	data['style'] = entity.style.toJson();
	data['tag'] =  entity.tag.map((v) => v.toJson()).toList();
	data['evaluate'] = entity.evaluate;
	data['ep'] =  entity.ep.map((v) => v.toJson()).toList();
	data['auto_pay_status'] = entity.autoPayStatus;
	data['copyright'] = entity.copyright.toJson();
	data['favorite'] = entity.favorite;
	data['discount'] =  entity.discount;
	data['status'] = entity.status;
	data['comment_id'] = entity.commentId;
	data['comment_state'] = entity.commentState;
	data['comment_share_state'] = entity.commentShareState;
	data['discount_state'] = entity.discountState;
	data['styles'] =  entity.styles.map((v) => v.toJson()).toList();
	return data;
}

CartoonModelDataAuthor $CartoonModelDataAuthorFromJson(Map<String, dynamic> json) {
	final CartoonModelDataAuthor cartoonModelDataAuthor = CartoonModelDataAuthor();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		cartoonModelDataAuthor.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		cartoonModelDataAuthor.name = name;
	}
	return cartoonModelDataAuthor;
}

Map<String, dynamic> $CartoonModelDataAuthorToJson(CartoonModelDataAuthor entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

CartoonModelDataCover $CartoonModelDataCoverFromJson(Map<String, dynamic> json) {
	final CartoonModelDataCover cartoonModelDataCover = CartoonModelDataCover();
	final String? vCover = jsonConvert.convert<String>(json['v_cover']);
	if (vCover != null) {
		cartoonModelDataCover.vCover = vCover;
	}
	final String? sCover = jsonConvert.convert<String>(json['s_cover']);
	if (sCover != null) {
		cartoonModelDataCover.sCover = sCover;
	}
	return cartoonModelDataCover;
}

Map<String, dynamic> $CartoonModelDataCoverToJson(CartoonModelDataCover entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['v_cover'] = entity.vCover;
	data['s_cover'] = entity.sCover;
	return data;
}

CartoonModelDataStyle $CartoonModelDataStyleFromJson(Map<String, dynamic> json) {
	final CartoonModelDataStyle cartoonModelDataStyle = CartoonModelDataStyle();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		cartoonModelDataStyle.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		cartoonModelDataStyle.name = name;
	}
	final int? pId = jsonConvert.convert<int>(json['p_id']);
	if (pId != null) {
		cartoonModelDataStyle.pId = pId;
	}
	return cartoonModelDataStyle;
}

Map<String, dynamic> $CartoonModelDataStyleToJson(CartoonModelDataStyle entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['p_id'] = entity.pId;
	return data;
}

CartoonModelDataTag $CartoonModelDataTagFromJson(Map<String, dynamic> json) {
	final CartoonModelDataTag cartoonModelDataTag = CartoonModelDataTag();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		cartoonModelDataTag.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		cartoonModelDataTag.name = name;
	}
	return cartoonModelDataTag;
}

Map<String, dynamic> $CartoonModelDataTagToJson(CartoonModelDataTag entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	return data;
}

CartoonModelDataEp $CartoonModelDataEpFromJson(Map<String, dynamic> json) {
	final CartoonModelDataEp cartoonModelDataEp = CartoonModelDataEp();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		cartoonModelDataEp.id = id;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		cartoonModelDataEp.title = title;
	}
	final String? shortTitle = jsonConvert.convert<String>(json['short_title']);
	if (shortTitle != null) {
		cartoonModelDataEp.shortTitle = shortTitle;
	}
	final int? payMode = jsonConvert.convert<int>(json['pay_mode']);
	if (payMode != null) {
		cartoonModelDataEp.payMode = payMode;
	}
	final bool? isLocked = jsonConvert.convert<bool>(json['is_locked']);
	if (isLocked != null) {
		cartoonModelDataEp.isLocked = isLocked;
	}
	final int? unlockType = jsonConvert.convert<int>(json['unlock_type']);
	if (unlockType != null) {
		cartoonModelDataEp.unlockType = unlockType;
	}
	final int? read = jsonConvert.convert<int>(json['read']);
	if (read != null) {
		cartoonModelDataEp.read = read;
	}
	final int? lastRead = jsonConvert.convert<int>(json['last_read']);
	if (lastRead != null) {
		cartoonModelDataEp.lastRead = lastRead;
	}
	final String? firstOnlineTime = jsonConvert.convert<String>(json['first_online_time']);
	if (firstOnlineTime != null) {
		cartoonModelDataEp.firstOnlineTime = firstOnlineTime;
	}
	return cartoonModelDataEp;
}

Map<String, dynamic> $CartoonModelDataEpToJson(CartoonModelDataEp entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['title'] = entity.title;
	data['short_title'] = entity.shortTitle;
	data['pay_mode'] = entity.payMode;
	data['is_locked'] = entity.isLocked;
	data['unlock_type'] = entity.unlockType;
	data['read'] = entity.read;
	data['last_read'] = entity.lastRead;
	data['first_online_time'] = entity.firstOnlineTime;
	return data;
}

CartoonModelDataCopyright $CartoonModelDataCopyrightFromJson(Map<String, dynamic> json) {
	final CartoonModelDataCopyright cartoonModelDataCopyright = CartoonModelDataCopyright();
	final int? limit = jsonConvert.convert<int>(json['limit']);
	if (limit != null) {
		cartoonModelDataCopyright.limit = limit;
	}
	return cartoonModelDataCopyright;
}

Map<String, dynamic> $CartoonModelDataCopyrightToJson(CartoonModelDataCopyright entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['limit'] = entity.limit;
	return data;
}

CartoonModelDataStyles $CartoonModelDataStylesFromJson(Map<String, dynamic> json) {
	final CartoonModelDataStyles cartoonModelDataStyles = CartoonModelDataStyles();
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		cartoonModelDataStyles.id = id;
	}
	final String? name = jsonConvert.convert<String>(json['name']);
	if (name != null) {
		cartoonModelDataStyles.name = name;
	}
	final int? pId = jsonConvert.convert<int>(json['p_id']);
	if (pId != null) {
		cartoonModelDataStyles.pId = pId;
	}
	return cartoonModelDataStyles;
}

Map<String, dynamic> $CartoonModelDataStylesToJson(CartoonModelDataStyles entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['id'] = entity.id;
	data['name'] = entity.name;
	data['p_id'] = entity.pId;
	return data;
}